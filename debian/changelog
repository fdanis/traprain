traprain (0.2019pre.0-0co1) apertis; urgency=medium

  * New release: 0.2019pre.0
  * debian/rules: Make compiler warnings non-fatal

 -- Emanuele Aina <emanuele.aina@collabora.com>  Wed, 24 Apr 2019 22:51:07 +0000

traprain (0.1812.0-0co1) 18.12; urgency=medium

  [ Luis Araujo ]
  * Add traprain debian autopkgtest

  [ Frédéric Dalleau ]
  * Update link to document
  * Update apparmor rules to take tests into accounts

 -- Frédéric Dalleau <frederic.dalleau@collabora.co.uk>  Tue, 16 Oct 2018 16:01:30 +0000

traprain (0.1703.1-0co1) 17 03; urgency=medium

  [ Guillaume Desmottes ]
  * doc: fix links
  * dbus: namespace reference to Notify() function
  * turn-by-turn-service: expose the object *before* claiming the bus name
  * TrpGuidanceTurnByTurnNotification: add 'updated' signal
  * add guidance-ui service
  * guidance-ui: display progress info (Apertis: T2814)
  * mock-service: add API to change Progress info
  * mock-service: add SystemdService key to service files
  * tools: add traprain-update-progress
  * tools: add traprain-send-turn-by-turn (Apertis: T2815)
  * README: mention Progress and TurnByTurn new API and tools (Apertis: T2816)
  * doc: update overview with new API

 -- Guillaume Desmottes <guillaume.desmottes@collabora.co.uk>  Mon, 02 Jan 2017 10:14:30 +0100

traprain (0.1612.2-0co1) 16.12; urgency=medium

  * debian: traprain-mock-service: add dep on 'acl'
  * mock-service: install D-Bus service files to system-services/

 -- Guillaume Desmottes <guillaume.desmottes@collabora.co.uk>  Thu, 15 Dec 2016 12:45:54 +0000

traprain (0.1612.1-0co1) 16.12; urgency=medium

  [ Frédéric Dalleau ]
  * arclint: add traprain-common directory
  * TrpRoutingRequest: add and test preferences API (Apertis: T2846)

 -- Frédéric Dalleau <frederic.dalleau@collabora.com>  Fri, 25 Nov 2016 15:34:30 +0000

traprain (0.1612.0-0co1) 16.12; urgency=medium

  [ Simon McVittie ]
  * Add debian/source/apertis-component marking this as a target package
  * Add the usual debug macros
  * Invoke lcov in a way that actually works

  [ Mathieu Duponchelle ]
  * Require hotdoc version 0.8
  * hotdoc: remove --gtk-doc-remove-xml

  [ Guillaume Desmottes ]
  * dbus: fix interface prefix when generating code
  * debian: override_dh_makeshlibs when building releases
  * Add org.apertis.NavigationGuidance1.Progress API (Apertis: T2809),
    TrpServiceNavigationGuidanceProgress and
    TrpClientNavigationGuidanceProgress (Apertis: T2813)
  * Add org.apertis.NavigationGuidance1.TurnByTurn API (Apertis: T2809),
    TrpServiceTurnByTurnNotification,
    TrpGuidanceTurnByTurnService and
    TrpGuidanceTurnByTurnNotification (Apertis: T2811)
  * mock-service: fix signature of 'handle-' signals

  [ Simon McVittie, Frédéric Dalleau ]
  * TrpPlace, TrpPlaceBuilder: add and test an API for places, which
    encapsulates geo: and place: URIs
  * TrpRoutingRequest: add and test routing requests, which encapsulate
    nav: URIs (Apertis: T2844)

 -- Simon McVittie <simon.mcvittie@collabora.co.uk>  Tue, 08 Nov 2016 11:56:59 +0000

traprain (0.1.0-0co1) 16.09; urgency=medium

  * 0.1.0 release

 -- Guillaume Desmottes <guillaume.desmottes@collabora.co.uk>  Wed, 14 Sep 2016 16:54:34 +0200
