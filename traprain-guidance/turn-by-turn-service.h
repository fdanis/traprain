/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_GUIDANCE_TURN_BY_TURN_SERVICE_H__
#define __TRAPRAIN_GUIDANCE_TURN_BY_TURN_SERVICE_H__

#include <gio/gio.h>
#include <glib-object.h>

#include "turn-by-turn-notification.h"

G_BEGIN_DECLS

#define TRP_GUIDANCE_TYPE_TURN_BY_TURN_SERVICE (trp_guidance_turn_by_turn_service_get_type ())
G_DECLARE_FINAL_TYPE (TrpGuidanceTurnByTurnService, trp_guidance_turn_by_turn_service, TRP_GUIDANCE, TURN_BY_TURN_SERVICE, GObject)

TrpGuidanceTurnByTurnService *trp_guidance_turn_by_turn_service_new (GDBusConnection *connection);

G_END_DECLS

#endif /* __TRAPRAIN_GUIDANCE_TURN_BY_TURN_SERVICE_H__ */
