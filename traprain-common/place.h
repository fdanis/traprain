/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_COMMON_PLACE_H__
#define __TRAPRAIN_COMMON_PLACE_H__

#include <glib-object.h>

G_BEGIN_DECLS

/**
 * TrpUriFlags:
 * @TRP_URI_FLAGS_NONE: No special behaviour
 * @TRP_URI_FLAGS_STRICT: Strictly follow RFC 5870 during parsing
 *
 * Flags influencing how we parse URIs. %TRP_URI_FLAGS_STRICT is more
 * appropriate when reading a URI that has been written out.
 *
 * Since: 0.1612.0
 */
typedef enum {
  TRP_URI_FLAGS_NONE = 0,
  TRP_URI_FLAGS_STRICT = (1 << 0),
} TrpUriFlags;

/**
 * TrpUriScheme:
 * %TRP_URI_SCHEME_ANY: return whatever URI best represents this place
 * %TRP_URI_SCHEME_GEO: a [`geo:` URI][RFC 5870] for physical locations
 * %TRP_URI_SCHEME_PLACE: a [`place:` URI][place URI] for physical locations,
 *  postal addresses, search terms or a combination of these
 * %TRP_URI_SCHEME_NAV: a [`nav:` URI][nav URI] for requests to calculate a
 *  route to a destination with particular parameters
 *
 * URI schemes used to represent navigation-related information.
 *
 * [RFC 5870]: https://tools.ietf.org/html/rfc5870
 * [place URI]: https://docs.apertis.org/latest/geolocation-and-navigation.html#appendix-place-uri-scheme
 * [nav URI]: https://docs.apertis.org/latest/geolocation-and-navigation.html#appendix-nav-uri-scheme
 *
 * Since: 0.1612.0
 */
typedef enum {
  TRP_URI_SCHEME_ANY,
  TRP_URI_SCHEME_GEO,
  TRP_URI_SCHEME_PLACE,
  TRP_URI_SCHEME_NAV,
} TrpUriScheme;

/**
 * TrpCoordinateReferenceSystem:
 * %TRP_COORDINATE_REFERENCE_SYSTEM_WGS84: The [World Geodetic System
 *  1984](http://earth-info.nga.mil/GandG/publications/tr8350.2/tr8350_2.html)
 *  (WGS-84), as used in GPS
 *
 * The coordinate reference system used as a mathematical model of the
 * Earth when interpreting the #TrpPlace:latitude, #TrpPlace:longitude and
 * #TrpPlace:altitude. WGS-84, the CRS used by the Global Positioning
 * System, is currently the only one supported.
 *
 * [The Wikipedia page on the World Geodetic
 * System](https://en.wikipedia.org/wiki/World_Geodetic_System) provides
 * a useful overview.
 *
 * Members of this enum correspond to the IANA registry of
 * ['geo' URI 'crs' Parameter
 * Values](https://www.iana.org/assignments/geo-uri-parameters/geo-uri-parameters.xhtml#geo-uri-parameters-2).
 *
 * Since: 0.1612.0
 */
typedef enum {
  TRP_COORDINATE_REFERENCE_SYSTEM_WGS84,
} TrpCoordinateReferenceSystem;

/**
 * TRP_PLACE_ERROR:
 *
 * Error domain corresponding to the #TrpPlaceError enumeration.
 *
 * Since: 0.1612.0
 */
#define TRP_PLACE_ERROR (trp_place_error_quark ())
GQuark trp_place_error_quark (void);

/**
 * TrpPlaceError:
 * @TRP_PLACE_ERROR_FAILED: Generic error
 * @TRP_PLACE_ERROR_INVALID_URI: A URI was syntactically invalid
 * @TRP_PLACE_ERROR_UNKNOWN_SCHEME: An unknown URI scheme was used
 * @TRP_PLACE_ERROR_UNKNOWN_CRS: An unknown coordinate reference system
 *  was used
 * @TRP_PLACE_ERROR_OUT_OF_RANGE: A coordinate was out of range, for
 *  example a latitude of 91°N
 * @TRP_PLACE_ERROR_INVALID_COUNTRY_CODE: A country code was not in
 *  ISO 3166 alpha-3 format (three letters, for example `GBR` or `fra`)
 *
 * An error in defining a place or parsing a navigation-related URI.
 *
 * Since: 0.1612.0
 */
typedef enum {
  TRP_PLACE_ERROR_FAILED,
  TRP_PLACE_ERROR_INVALID_URI,
  TRP_PLACE_ERROR_UNKNOWN_SCHEME,
  TRP_PLACE_ERROR_UNKNOWN_CRS,
  TRP_PLACE_ERROR_OUT_OF_RANGE,
  TRP_PLACE_ERROR_INVALID_COUNTRY_CODE,
} TrpPlaceError;

#define TRP_TYPE_PLACE (trp_place_get_type ())
G_DECLARE_FINAL_TYPE (TrpPlace, trp_place, TRP, PLACE, GObject)

TrpPlace *trp_place_new_for_uri (const gchar *uri,
                                 TrpUriFlags flags,
                                 GError **error);

gchar *trp_place_to_uri (TrpPlace *self,
                         TrpUriScheme scheme);
const gchar *trp_place_get_location_string (TrpPlace *self);
gchar *trp_place_force_location_string (TrpPlace *self);
gdouble trp_place_get_latitude (TrpPlace *self);
gdouble trp_place_get_longitude (TrpPlace *self);
gdouble trp_place_get_altitude (TrpPlace *self);
gdouble trp_place_get_uncertainty (TrpPlace *self);
const gchar *trp_place_get_postal_code (TrpPlace *self);
const gchar *trp_place_get_country_code (TrpPlace *self);
const gchar *trp_place_get_region (TrpPlace *self);
const gchar *trp_place_get_locality (TrpPlace *self);
const gchar *trp_place_get_area (TrpPlace *self);
const gchar *trp_place_get_street (TrpPlace *self);
const gchar *trp_place_get_building (TrpPlace *self);
const gchar *trp_place_get_formatted_address (TrpPlace *self);
TrpCoordinateReferenceSystem trp_place_get_crs (TrpPlace *self);

/**
 * TRP_PLACE_LATITUDE_UNKNOWN:
 *
 * Value representing an unknown latitude.
 *
 * Since: 0.1612.0
 */
#define TRP_PLACE_LATITUDE_UNKNOWN (-G_MAXDOUBLE)

/**
 * TRP_PLACE_LONGITUDE_UNKNOWN:
 *
 * Value representing an unknown longitude.
 *
 * Since: 0.1612.0
 */
#define TRP_PLACE_LONGITUDE_UNKNOWN (-G_MAXDOUBLE)

/**
 * TRP_PLACE_ALTITUDE_UNKNOWN:
 *
 * Value representing an unknown altitude.
 *
 * Since: 0.1612.0
 */
#define TRP_PLACE_ALTITUDE_UNKNOWN (-G_MAXDOUBLE)

/**
 * TRP_PLACE_UNCERTAINTY_UNKNOWN:
 *
 * Value representing an unknown uncertainty. An exact point has an
 * uncertainty value of 0.
 *
 * Since: 0.1612.0
 */
#define TRP_PLACE_UNCERTAINTY_UNKNOWN (-G_MAXDOUBLE)

/**
 * trp_place_is_latitude:
 * @d: a potential latitude
 *
 * Check whether @d is a valid latitude. Valid latitudes range from
 * -90 degrees (the South Pole) to +90 degrees (the North Pole), inclusive.
 * %TRP_PLACE_LATITUDE_UNKNOWN is not considered to be a valid latitude.
 *
 * Returns: %TRUE if @d is between -90 and 90 degrees, inclusive
 * Since: 0.1612.0
 */
static inline gboolean
_trp_place_is_latitude (gdouble d)
{
  return (d >= -90 && d <= 90);
}
#define trp_place_is_latitude(d) (_trp_place_is_latitude (d))
gboolean (trp_place_is_latitude) (gdouble d);

/**
 * trp_place_is_longitude:
 * @d: a potential longitude
 *
 * Check whether @d is a valid longitude. Valid longitudes range from
 * -180 degrees (180 degrees west of Greenwich) to +180 degrees
 *  (180 degrees east of Greenwich), inclusive.
 * %TRP_PLACE_LONGITUDE_UNKNOWN is not considered to be a valid longitude.
 *
 * Returns: %TRUE if @d is between -180 and 180 degrees, inclusive
 * Since: 0.1612.0
 */
static inline gboolean
_trp_place_is_longitude (gdouble d)
{
  return (d >= -180 && d <= 180);
}
#define trp_place_is_longitude(d) (_trp_place_is_longitude (d))
gboolean (trp_place_is_longitude) (gdouble d);

/**
 * trp_place_is_altitude:
 * @d: a potential altitude
 *
 * Check whether @d is a valid altitude.
 * %TRP_PLACE_ALTITUDE_UNKNOWN is not considered to be a valid altitude.
 *
 * Returns: %TRUE if @d represents a known altitude
 * Since: 0.1612.0
 */
/* https://en.wikipedia.org/wiki/Earth_radius, rounding up */
#define trp_place_is_altitude(d) ((d) > -6.4e6)
gboolean (trp_place_is_altitude) (gdouble d);

/**
 * trp_place_is_uncertainty:
 * @d: a potential altitude
 *
 * Check whether @d is a valid uncertainty. Valid uncertainties are
 * non-negative numbers in metres; %TRP_PLACE_UNCERTAINTY_UNKNOWN is not
 * considered to be a valid uncertainty.
 *
 * Returns: %TRUE if @d represents a known uncertainty
 * Since: 0.1612.0
 */
#define trp_place_is_uncertainty(d) ((d) >= 0)
gboolean (trp_place_is_uncertainty) (gdouble d);

gboolean trp_place_is_country_code (const gchar *country);

typedef struct _TrpPlaceBuilder TrpPlaceBuilder;

#define TRP_TYPE_PLACE_BUILDER (trp_place_builder_get_type ())
GType trp_place_builder_get_type (void);

TrpPlaceBuilder *trp_place_builder_new (TrpPlace *place);
TrpPlaceBuilder *trp_place_builder_ref (TrpPlaceBuilder *self);
void trp_place_builder_unref (TrpPlaceBuilder *self);
G_DEFINE_AUTOPTR_CLEANUP_FUNC (TrpPlaceBuilder, trp_place_builder_unref)

void trp_place_builder_set_latitude (TrpPlaceBuilder *self,
                                     gdouble latitude);
void trp_place_builder_set_longitude (TrpPlaceBuilder *self,
                                      gdouble longitude);
void trp_place_builder_set_altitude (TrpPlaceBuilder *self,
                                     gdouble altitude);
void trp_place_builder_set_uncertainty (TrpPlaceBuilder *self,
                                        gdouble uncertainty);
void trp_place_builder_set_location_string (TrpPlaceBuilder *self,
                                            const gchar *location_string);
void trp_place_builder_set_postal_code (TrpPlaceBuilder *self,
                                        const gchar *postal_code);
void trp_place_builder_set_country_code (TrpPlaceBuilder *self,
                                         const gchar *country);
void trp_place_builder_set_region (TrpPlaceBuilder *self,
                                   const gchar *region);
void trp_place_builder_set_locality (TrpPlaceBuilder *self,
                                     const gchar *locality);
void trp_place_builder_set_area (TrpPlaceBuilder *self,
                                 const gchar *area);
void trp_place_builder_set_street (TrpPlaceBuilder *self,
                                   const gchar *street);
void trp_place_builder_set_building (TrpPlaceBuilder *self,
                                     const gchar *building);
void trp_place_builder_set_formatted_address (TrpPlaceBuilder *self,
                                              const gchar *address);
TrpPlace *trp_place_builder_copy (TrpPlaceBuilder *self);
TrpPlace *trp_place_builder_end (TrpPlaceBuilder *self);

G_END_DECLS

#endif
