/*<private_header>*/
/*
 * Copyright (C) 2004 Red Hat, Inc.
 * Copyright (C) 2006 Alberto Ruiz
 * Copyright (C) 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: LGPL-2+
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * On Apertis systems, a copy of the GNU Lesser General Public License
 * version 2 can be found in <file:///usr/share/common-licenses/LGPL-2>.
 */

#ifndef __TRAPRAIN_COMMON_GLIB_DERIVED_H__
#define __TRAPRAIN_COMMON_GLIB_DERIVED_H__

#include <glib.h>

G_BEGIN_DECLS

gchar *_trp_utf8_make_valid (const gchar *str);

G_END_DECLS

#endif
