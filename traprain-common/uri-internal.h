/*<private_header>*/
/* vim: set sts=2 sw=2 et : */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_COMMON_URI_INTERNAL_H__
#define __TRAPRAIN_COMMON_URI_INTERNAL_H__

#include <glib.h>

#include "traprain-common/place.h"

G_BEGIN_DECLS

/* Character classes from https://tools.ietf.org/html/rfc5870#section-3.3 */
#define TRP_URI_RFC5870_MARK "-_.!~*'()"
#define TRP_URI_RFC5870_P_UNRESERVED "[]:&+$"

gboolean _trp_uri_consume_double (const gchar **p,
                                  gdouble *value,
                                  GError **error);
gboolean _trp_uri_consume_label_text (const gchar **p,
                                      gchar **value,
                                      GError **error);
gboolean _trp_uri_consume_parameter_value (const gchar **p,
                                           TrpUriFlags flags,
                                           gchar **value,
                                           GError **error);

void _trp_uri_append_parameter_value (GString *string,
                                      const gchar *unescaped_value);

G_END_DECLS

#endif
