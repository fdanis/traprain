/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "route-internal.h"
#include "route.h"

G_DEFINE_TYPE (TrpRoute, trp_route, G_TYPE_OBJECT)

/**
 * SECTION: traprain-common/route.h
 * @title: TrpRoute
 * @short_description: Navigation routes to expose on the bus
 * @see_also: #TrpServiceNavigation #TrpClientMock
 *
 * #TrpRoute is used to represent a potential navigation route in order to expose
 * it on D-Bus.
 *
 * Routes are defined by ordered segments from the start point
 * to the destination. Use trp_route_add_segment()
 * and trp_route_add_segment_description() to build the
 * route.
 *
 * Route objects should no longer be modified once they have been
 * been exposed on the bus, generally by passing it to a
 * #TrpServiceNavigation.
 *
 * Since: 0.1.0
 */

/**
 * TrpRoute:
 *
 * Object representing a potential navigation route to expose on the bus.
 *
 * Since: 0.1.0
 */

static _TrpSegment *
segment_new (gdouble latitude,
             gdouble longitude)
{
  _TrpSegment *s = g_slice_new0 (_TrpSegment);

  s->latitude = latitude;
  s->longitude = longitude;
  s->description = _trp_languages_map_new ();

  return s;
}

static void
segment_free (_TrpSegment *s)
{
  g_array_unref (s->description);
  g_slice_free (_TrpSegment, s);
}

static void
trp_route_init (TrpRoute *self)
{
  self->geometry = g_ptr_array_new_with_free_func ((GDestroyNotify) segment_free);
  self->title = _trp_languages_map_new ();

  self->writable = TRUE;
}

static void
trp_route_dispose (GObject *object)
{
  TrpRoute *route = (TrpRoute *) object;

  g_clear_pointer (&route->title, g_array_unref);
  g_clear_pointer (&route->geometry, g_ptr_array_unref);

  G_OBJECT_CLASS (trp_route_parent_class)
      ->dispose (object);
}

static void
trp_route_class_init (TrpRouteClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->dispose = trp_route_dispose;
}

TrpRoute *
_trp_route_new (guint total_distance,
                guint total_time)
{
  TrpRoute *route;

  route = g_object_new (TRP_TYPE_ROUTE, NULL);
  route->total_distance = total_distance;
  route->total_time = total_time;

  return route;
}

/**
 * trp_route_add_title:
 * @route: a #TrpRoute
 * @language: a language name code, in te POSIX locale format (ISO 15897)
 * @title: a human-readable title of @route in @language
 *
 * Set a human-readable title of @route in @language.
 * @language should be in the POSIX locale format with locale identifiers
 * defined by ISO 15897, like fr_BE for example.
 *
 * You should not call this function once @route has been exposed on the bus.
 *
 * Since: 0.1.0
 */
void
trp_route_add_title (TrpRoute *route,
                     const gchar *language,
                     const gchar *title)
{
  g_return_if_fail (route != NULL);
  g_return_if_fail (language != NULL);
  g_return_if_fail (title != NULL);
  g_return_if_fail (route->writable);

  _trp_languages_map_add (route->title, language, title);
}

/**
 * trp_route_add_segment:
 * @route: a #TrpRoute
 * @latitude: the latitude of the segment, between -90.0 and 90.0
 * @longitude: the longitude of the segment, between -180.0 and 180.0
 *
 * Add a segment to @route at the location defined by @latitude and @longitude.
 * You should then use trp_route_add_segment_description() with the index
 * returned by this function to provide a human-readable description of this
 * segment, if available.
 *
 * You should not call this function once @route has been exposed on the bus.
 *
 * Returns: an index representing the position of this segment in @route
 * Since: 0.1.0
 */
guint
trp_route_add_segment (TrpRoute *route,
                       gdouble latitude,
                       gdouble longitude)
{
  g_return_val_if_fail (route != NULL, 0);
  g_return_val_if_fail (latitude >= -90.0 && latitude <= 90.0, 0);
  g_return_val_if_fail (longitude >= -180.0 && longitude <= 180.0, 0);
  g_return_val_if_fail (route->writable, 0);

  g_ptr_array_add (route->geometry, segment_new (latitude, longitude));

  return route->geometry->len - 1;
}

/**
 * trp_route_add_segment_description:
 * @route: a #TrpRoute
 * @index: the index of an existing segment in @route
 * @language: a language name code, in te POSIX locale format (ISO 15897)
 * @description: a human-readable description of this segment in @language
 *
 * Set a human-readable description of the segment indexed at @index
 * of @route.
 * @language should be in the POSIX locale format with locale identifiers
 * defined by ISO 15897, like fr_BE for example.
 *
 * You should not call this function once @route has been exposed on the bus.
 *
 * Since: 0.1.0
 */
void
trp_route_add_segment_description (TrpRoute *route,
                                   guint index,
                                   const gchar *language,
                                   const gchar *description)
{
  _TrpSegment *s;

  g_return_if_fail (route != NULL);
  g_return_if_fail (index < route->geometry->len);
  g_return_if_fail (language != NULL);
  g_return_if_fail (description != NULL);
  g_return_if_fail (route->writable);

  s = g_ptr_array_index (route->geometry, index);
  _trp_languages_map_add (s->description, language, description);
}

void
_trp_route_to_variants (TrpRoute *route,
                        GVariant **title,
                        GVariant **geometry,
                        GVariant **geometry_desc)
{
  GVariantBuilder geometry_builder, desc_builder;
  guint i;

  g_return_if_fail (TRP_IS_ROUTE (route));
  g_return_if_fail (title != NULL);
  g_return_if_fail (geometry != NULL);
  g_return_if_fail (geometry_desc != NULL);

  *title = _trp_languages_map_to_variant (route->title);

  g_variant_builder_init (&geometry_builder, G_VARIANT_TYPE ("a(dd)"));
  g_variant_builder_init (&desc_builder, G_VARIANT_TYPE ("a{ua{ss}}"));

  for (i = 0; i < route->geometry->len; i++)
    {
      _TrpSegment *s = g_ptr_array_index (route->geometry, i);

      g_variant_builder_add (&geometry_builder, "(dd)", s->latitude, s->longitude);

      /* add descriptions */
      if (s->description->len > 0)
        {
          g_variant_builder_add_value (&desc_builder, g_variant_new_dict_entry (
                                                          g_variant_new_uint32 (i),
                                                          _trp_languages_map_to_variant (s->description)));
        }
    }

  *geometry = g_variant_builder_end (&geometry_builder);
  *geometry_desc = g_variant_builder_end (&desc_builder);
}
