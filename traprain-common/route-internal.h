/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_COMMON_ROUTE_INTERNAL_H__
#define __TRAPRAIN_COMMON_ROUTE_INTERNAL_H__

#include <glib-object.h>

#include "route.h"

#include "languages-map-internal.h"

G_BEGIN_DECLS

typedef struct
{
  gdouble latitude;
  gdouble longitude;
  TrpLanguagesMap *description; /* owned */
} _TrpSegment;

struct _TrpRoute
{
  GObject parent;

  guint total_distance;
  guint total_time;
  TrpLanguagesMap *title;                        /* owned */
  GPtrArray /* <owned _TrpSegment> */ *geometry; /* owned */

  gboolean writable;
};

TrpRoute *_trp_route_new (guint total_distance,
                          guint total_time);

void _trp_route_to_variants (TrpRoute *route,
                             GVariant **title,
                             GVariant **geometry,
                             GVariant **geometry_desc);

G_END_DECLS

#endif /* __TRAPRAIN_COMMON_ROUTE_INTERNAL_H__ */
