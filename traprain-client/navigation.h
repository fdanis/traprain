/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_CLIENT_NAVIGATION_H__
#define __TRAPRAIN_CLIENT_NAVIGATION_H__

#include <gio/gio.h>
#include <glib-object.h>

#include "route.h"

G_BEGIN_DECLS

#define TRP_CLIENT_TYPE_NAVIGATION (trp_client_navigation_get_type ())
G_DECLARE_FINAL_TYPE (TrpClientNavigation, trp_client_navigation, TRP_CLIENT, NAVIGATION, GObject)

TrpClientNavigation *trp_client_navigation_new (GDBusConnection *connection);

GPtrArray *trp_client_navigation_get_routes (TrpClientNavigation *self);

TrpClientRoute *trp_client_navigation_get_current_route (TrpClientNavigation *self);

G_END_DECLS

#endif /* __TRAPRAIN_CLIENT_NAVIGATION_H__ */
