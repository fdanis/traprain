/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_CLIENT_ROUTE_H__
#define __TRAPRAIN_CLIENT_ROUTE_H__

#include <gio/gio.h>
#include <glib-object.h>

#include "traprain-common/languages-map.h"

G_BEGIN_DECLS

#define TRP_CLIENT_TYPE_ROUTE (trp_client_route_get_type ())
G_DECLARE_FINAL_TYPE (TrpClientRoute, trp_client_route, TRP_CLIENT, ROUTE, GObject)

guint trp_client_route_get_total_distance (TrpClientRoute *self);
guint trp_client_route_get_total_time (TrpClientRoute *self);
TrpLanguagesMap *trp_client_route_get_titles (TrpClientRoute *self);
const gchar *trp_client_route_get_title (TrpClientRoute *self, const gchar *language);

guint trp_client_route_get_n_segments (TrpClientRoute *self);
void trp_client_route_get_segment (TrpClientRoute *self, guint index, gdouble *lat, gdouble *lon, TrpLanguagesMap **descriptions);
void trp_client_route_get_segment_in_language (TrpClientRoute *self, guint index, const gchar *language, gdouble *lat, gdouble *lon, const gchar **description);

G_END_DECLS

#endif /* __TRAPRAIN_CLIENT_ROUTE_H__ */
