/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_CLIENT_ROUTE_INTERNAL_H__
#define __TRAPRAIN_CLIENT_ROUTE_INTERNAL_H__

#include "route.h"

#include "dbus/org.apertis.Navigation1.h"
#include "traprain-common/languages-map-internal.h"

G_BEGIN_DECLS

struct _TrpClientRoute
{
  GObject parent;

  TrpNavigation1Route *proxy; /* owned */

  GHashTable /* <guint> -> <owned _TrpLanguagesMap *> */ *geo_desc; /* owned */
  GHashTable /* <guint> -> <owned GHashTable *> */ *geo_desc_hash;  /* owned */
  TrpLanguagesMap *title;                                           /* owned */

  /* ID used by TrpClientNavigation to identify route objects which are no
   * longer exposed by the service and so can be destroyed. */
  guint update_id;
};

TrpClientRoute *_trp_client_route_new (TrpNavigation1Route *proxy);

G_END_DECLS

#endif /* __TRAPRAIN_CLIENT_ROUTE_INTERNAL_H__ */
