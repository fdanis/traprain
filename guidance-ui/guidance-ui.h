/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_GUIDANCE_UI_H__
#define __TRAPRAIN_GUIDANCE_UI_H__

#include <gio/gio.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define TRP_GUIDANCE_TYPE_SERVICE (trp_guidance_ui_get_type ())
G_DECLARE_FINAL_TYPE (TrpGuidanceUI, trp_guidance_ui, TRP_GUIDANCE, UI, GApplication)

TrpGuidanceUI *
trp_guidance_ui_new (const gchar *application_id,
                     GApplicationFlags flags);

gboolean
trp_guidance_ui_start (TrpGuidanceUI *guidance);

void
trp_guidance_ui_stop (TrpGuidanceUI *guidance);

G_END_DECLS

#endif /* __TRAPRAIN_GUIDANCE_UI_H__ */
