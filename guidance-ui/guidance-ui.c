/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "guidance-ui.h"

#include <gio/gio.h>
#include <glib/gprintf.h>
#include <libnotify/notify.h>
#include <systemd/sd-daemon.h>

#include "traprain-client/navigation-guidance-progress.h"
#include "traprain-guidance/turn-by-turn-service.h"

#ifndef NotifyNotification_autoptr
G_DEFINE_AUTOPTR_CLEANUP_FUNC (NotifyNotification, g_object_unref)
#endif /* NotifyNotification_autoptr */

struct _TrpGuidanceUI
{
  GApplication parent;

  TrpGuidanceTurnByTurnService *service; /* owned */
  TrpClientNavigationGuidanceProgress *progress;
  GDBusConnection *sys_conn;
  GCancellable *cancellable;                                                                                       /* owned */
  GHashTable /* <owned TrpGuidanceTurnByTurnNotification> -> <owned NotifyNotification> */ *current_notifications; /* owned */
};

G_DEFINE_TYPE (TrpGuidanceUI, trp_guidance_ui, G_TYPE_APPLICATION)

static void
trp_guidance_ui_dispose (GObject *object)
{
  TrpGuidanceUI *self = (TrpGuidanceUI *) object;

  g_clear_pointer (&self->current_notifications, g_hash_table_unref);

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  g_clear_object (&self->service);
  g_clear_object (&self->progress);
  g_clear_object (&self->sys_conn);

  G_OBJECT_CLASS (trp_guidance_ui_parent_class)
      ->dispose (object);
}

static void
trp_guidance_ui_class_init (TrpGuidanceUIClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->dispose = trp_guidance_ui_dispose;
}

static uint
get_timeout (TrpGuidanceTurnByTurnNotification *tbt_notification)
{
  gint expire_timeout;

  g_object_get (tbt_notification,
                "expire-timeout", &expire_timeout,
                NULL);

  if (expire_timeout == 0)
    return NOTIFY_EXPIRES_NEVER;
  else if (expire_timeout <= -1)
    return NOTIFY_EXPIRES_DEFAULT;

  return expire_timeout;
}

static void
on_handle_close (TrpGuidanceTurnByTurnNotification *tbt_notification,
                 TrpGuidanceUI *self)
{
  NotifyNotification *notification;
  g_autoptr (GError) error = NULL;

  g_debug ("Closing notification %p", tbt_notification);

  notification = g_hash_table_lookup (self->current_notifications,
                                      tbt_notification);
  if (notification == NULL)
    /* Notification has already been closed */
    return;

  if (!notify_notification_close (notification, &error))
    g_warning ("Failed to close notification: %s", error->message);
}

static gboolean create_and_show_notification (TrpGuidanceUI *self,
                                              TrpGuidanceTurnByTurnNotification *tbt_notification);

static void
notification_updated (TrpGuidanceTurnByTurnNotification *tbt_notification,
                      TrpGuidanceUI *self)
{
  NotifyNotification *notification;
  g_autofree gchar *summary = NULL;
  g_autofree gchar *body = NULL;
  g_autofree gchar *icon_name = NULL;
  g_autoptr (GError) error = NULL;

  notification = g_hash_table_lookup (self->current_notifications, tbt_notification);
  if (notification == NULL)
    {
      /* Notification has already been closed, create a new one */
      create_and_show_notification (self, tbt_notification);
      /* The 'close' and 'updated' signals on @tbt_notification have already be connected */
      return;
    }

  g_object_get (tbt_notification,
                "summary", &summary,
                "body", &body,
                "icon", &icon_name,
                NULL);

  notify_notification_update (notification, summary, body, icon_name);
  notify_notification_set_timeout (notification, get_timeout (tbt_notification));

  if (!notify_notification_show (notification, &error))
    {
      g_warning ("Failed to show updated notification: %s", error->message);
      return;
    }
}

static void
notification_closed_cb (NotifyNotification *notification,
                        TrpGuidanceUI *self)
{
  GHashTableIter iter;
  gpointer k, v;

  g_hash_table_iter_init (&iter, self->current_notifications);
  while (g_hash_table_iter_next (&iter, &k, &v))
    {
      if (v == notification)
        {
          g_hash_table_iter_remove (&iter);
          return;
        }
    }
}

static gboolean
create_and_show_notification (TrpGuidanceUI *self,
                              TrpGuidanceTurnByTurnNotification *tbt_notification)
{
  g_autoptr (NotifyNotification) notification = NULL;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *summary = NULL;
  g_autofree gchar *body = NULL;
  g_autofree gchar *icon_name = NULL;

  g_object_get (tbt_notification,
                "summary", &summary,
                "body", &body,
                "icon", &icon_name,
                NULL);

  g_debug ("Creating notification %p", tbt_notification);

  notification = notify_notification_new (summary, body, icon_name);
  notify_notification_set_timeout (notification, get_timeout (tbt_notification));

  g_signal_connect (notification, "closed", G_CALLBACK (notification_closed_cb), self);

  if (!notify_notification_show (notification, &error))
    {
      g_warning ("Failed to show notification: %s", error->message);
      return FALSE;
    }

  g_hash_table_insert (self->current_notifications,
                       g_object_ref (tbt_notification),
                       g_steal_pointer (&notification));

  return TRUE;
}

static void
new_notification_cb (TrpGuidanceTurnByTurnService *service,
                     TrpGuidanceTurnByTurnNotification *tbt_notification,
                     TrpGuidanceUI *self)
{
  if (!create_and_show_notification (self, tbt_notification))
    return;

  g_signal_connect (tbt_notification, "close",
                    G_CALLBACK (on_handle_close), self);
  g_signal_connect (tbt_notification, "updated",
                    G_CALLBACK (notification_updated), self);
}

static gchar *
date_time_to_str (GDateTime *d)
{
  if (d == NULL)
    return g_strdup ("undefined");

  return g_date_time_format (d, "%x %X");
}

static void
display_journey_time (TrpGuidanceUI *self)
{
  g_autofree gchar *start = NULL, *end = NULL, *tmp = NULL;
  g_autoptr (NotifyNotification) notification = NULL;
  g_autoptr (GError) error = NULL;

  start = date_time_to_str (trp_client_navigation_guidance_progress_get_start_time (self->progress));
  end = date_time_to_str (trp_client_navigation_guidance_progress_get_estimated_end_time (self->progress));

  tmp = g_strdup_printf ("Start time: %s\nEstimated end time: %s", start, end);

  notification = notify_notification_new ("Journey info updated", tmp, NULL);

  if (!notify_notification_show (notification, &error))
    g_warning ("Failed to show progress notification: %s", error->message);
}

static void
progress_init_cb (GObject *source,
                  GAsyncResult *result,
                  gpointer user_data)
{
  TrpGuidanceUI *self = user_data;
  g_autoptr (GError) error = NULL;

  if (!g_async_initable_init_finish (G_ASYNC_INITABLE (source), result, &error))
    {
      g_warning ("Failed to initialize Progress client: %s", error->message);
      return;
    }

  g_debug ("guidance UI service started");
  sd_notify (0, "READY=1");

  display_journey_time (self);
}

static void
progress_updated_cb (GObject *object,
                     GParamSpec *spec,
                     TrpGuidanceUI *self)
{
  display_journey_time (self);
}

static void
get_sys_bus_cb (GObject *source,
                GAsyncResult *result,
                gpointer user_data)
{
  TrpGuidanceUI *self = user_data;
  g_autoptr (GError) error = NULL;

  self->sys_conn = g_bus_get_finish (result, &error);
  if (self->sys_conn == NULL)
    {
      g_warning ("Failed to connect to system bus: %s", error->message);
      return;
    }

  self->progress = trp_client_navigation_guidance_progress_new (self->sys_conn);

  g_signal_connect (self->progress, "notify::start-time",
                    G_CALLBACK (progress_updated_cb), self);
  g_signal_connect (self->progress, "notify::estimated-end-time",
                    G_CALLBACK (progress_updated_cb), self);

  g_async_initable_init_async (G_ASYNC_INITABLE (self->progress),
                               G_PRIORITY_DEFAULT, self->cancellable,
                               progress_init_cb, self);
}

static void
service_init_cb (GObject *source,
                 GAsyncResult *result,
                 gpointer user_data)
{
  TrpGuidanceUI *self = user_data;
  g_autoptr (GError) error = NULL;

  if (!g_async_initable_init_finish (G_ASYNC_INITABLE (source), result, &error))
    {
      g_warning ("Failed to initialize turn-by-turn service: %s", error->message);
      return;
    }

  /* The Progress API is on the system bus */
  g_bus_get (G_BUS_TYPE_SYSTEM, self->cancellable, get_sys_bus_cb, self);
}

static void
trp_guidance_ui_init (TrpGuidanceUI *self)
{
  self->current_notifications = g_hash_table_new_full (NULL, NULL,
                                                       g_object_unref,
                                                       g_object_unref);
}

TrpGuidanceUI *
trp_guidance_ui_new (const gchar *application_id,
                     GApplicationFlags flags)
{
  g_return_val_if_fail (g_application_id_is_valid (application_id), NULL);

  return g_object_new (TRP_GUIDANCE_TYPE_SERVICE,
                       "application-id", application_id,
                       "flags", flags,
                       NULL);
}

gboolean
trp_guidance_ui_start (TrpGuidanceUI *self)
{
  GDBusConnection *conn = NULL;
  GApplication *application = G_APPLICATION (self);

  if (self->service != NULL)
    return TRUE;

  conn = g_application_get_dbus_connection (application);
  if (conn == NULL)
    {
      g_warning ("Failed to get D-Bus connection from GApplication");
      return FALSE;
    }

  self->service = trp_guidance_turn_by_turn_service_new (conn);

  g_signal_connect (self->service, "new-notification",
                    G_CALLBACK (new_notification_cb), self);

  g_async_initable_init_async (G_ASYNC_INITABLE (self->service),
                               G_PRIORITY_DEFAULT, self->cancellable,
                               service_init_cb, self);

  return TRUE;
}

void
trp_guidance_ui_stop (TrpGuidanceUI *self)
{
  sd_notify (0, "STOPPING=1");
}
