/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_SERVICE_ROUTE_INTERNAL_H__
#define __TRAPRAIN_SERVICE_ROUTE_INTERNAL_H__

#include "dbus/org.apertis.Navigation1.h"
#include "navigation.h"
#include "traprain-common/languages-map-internal.h"
#include "traprain-common/route-internal.h"

G_BEGIN_DECLS

#define _TRP_SERVICE_TYPE_ROUTE (_trp_service_route_get_type ())
G_DECLARE_FINAL_TYPE (_TrpServiceRoute, _trp_service_route, _TRP_SERVICE, ROUTE, TrpRoute)

struct __TrpServiceRoute
{
  TrpRoute parent;

  TrpObjectSkeleton *object;    /* owned */
  TrpNavigation1Route *service; /* owned */

  guint update_id;
};

TrpRoute *_trp_service_route_new (guint total_distance,
                                  guint total_time);

G_END_DECLS

#endif /* __TRAPRAIN_SERVICE_ROUTE_INTERNAL_H__ */
