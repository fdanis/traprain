/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "route-internal.h"

G_DEFINE_TYPE (_TrpServiceRoute, _trp_service_route, TRP_TYPE_ROUTE)

/**
 * _TrpServiceRoute:
 *
 * Object representing a potential navigation route to expose using
 * #TrpServiceNavigation.
 *
 * Use the #TrpRoute API to build the route: trp_route_add_title(),
 * trp_route_add_segment(), trp_route_add_segment_description().
 *
 * Since: 0.1.0
 */

static void
_trp_service_route_init (_TrpServiceRoute *self)
{
}

static void
_trp_service_route_dispose (GObject *object)
{
  _TrpServiceRoute *route = (_TrpServiceRoute *) object;

  g_clear_object (&route->service);
  g_clear_object (&route->object);

  G_OBJECT_CLASS (_trp_service_route_parent_class)
      ->dispose (object);
}

static void
_trp_service_route_class_init (_TrpServiceRouteClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->dispose = _trp_service_route_dispose;
}

TrpRoute *
_trp_service_route_new (guint total_distance,
                        guint total_time)
{
  TrpRoute *route = g_object_new (_TRP_SERVICE_TYPE_ROUTE, NULL);

  route->total_distance = total_distance;
  route->total_time = total_time;

  return route;
}
