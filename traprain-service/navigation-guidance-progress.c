/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "navigation-guidance-progress.h"

#include "dbus/org.apertis.NavigationGuidance1.Progress.h"

#define PROGRESS_PATH "/org/apertis/NavigationGuidance1/Progress"
#define PROGRESS_BUS_NAME "org.apertis.NavigationGuidance1.Progress"

/**
 * SECTION: traprain-service/navigation-guidance-progress.h
 * @title: TrpServiceNavigationGuidanceProgress
 * @short_description: Publish progress information about the current journey
 *
 * #TrpServiceNavigationGuidanceProgress is used to publish progress information
 * about the current journey in the navigation system.
 *
 * A navigation service should create one #TrpServiceNavigationGuidanceProgress
 * during its whole lifetime and use it to publish progress information to
 * external applications.
 * The information about the journey routes are published using #TrpServiceNavigation.
 *
 * Note that nothing is published until g_async_initable_init_async()
 * has been called.
 *
 * Since: 0.1612.0
 */

/**
 * TrpServiceNavigationGuidanceProgress:
 *
 * Object used to publish progress information about the current journey to
 * navigation UIs and third party applications.
 *
 * Since: 0.1612.0
 */

struct _TrpServiceNavigationGuidanceProgress
{
  GObject parent;

  TrpNavigationGuidance1Progress *service; /* owned */
  GDBusConnection *conn;                   /* owned */
  GCancellable *cancellable;               /* owned */
  GList *init_tasks;                       /* owned */
  gboolean registered;
  guint own_name_id;         /* 0 if the server is not registered yet */
  GError *invalidated_error; /* owned, NULL until the service is invalidated */
};

typedef enum {
  PROP_CONNECTION = 1,
  /*< private >*/
  PROP_LAST = PROP_CONNECTION
} _TrpServiceNavigationGuidanceProgressProperty;

static GParamSpec *properties[PROP_LAST + 1];

enum
{
  SIG_INVALIDATED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data);

G_DEFINE_TYPE_WITH_CODE (TrpServiceNavigationGuidanceProgress, trp_service_navigation_guidance_progress, G_TYPE_OBJECT, G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, async_initable_iface_init));

static void
trp_service_navigation_guidance_progress_get_property (GObject *object,
                                                       guint prop_id,
                                                       GValue *value,
                                                       GParamSpec *pspec)
{
  TrpServiceNavigationGuidanceProgress *self = TRP_SERVICE_NAVIGATION_GUIDANCE_PROGRESS (object);

  switch ((_TrpServiceNavigationGuidanceProgressProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, self->conn);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_service_navigation_guidance_progress_set_property (GObject *object,
                                                       guint prop_id,
                                                       const GValue *value,
                                                       GParamSpec *pspec)
{
  TrpServiceNavigationGuidanceProgress *self = TRP_SERVICE_NAVIGATION_GUIDANCE_PROGRESS (object);

  switch ((_TrpServiceNavigationGuidanceProgressProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_assert (self->conn == NULL); /* construct only */
      self->conn = g_value_dup_object (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_service_navigation_dispose (GObject *object)
{
  TrpServiceNavigationGuidanceProgress *self = (TrpServiceNavigationGuidanceProgress *) object;

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  if (self->own_name_id != 0)
    {
      g_bus_unown_name (self->own_name_id);
      self->own_name_id = 0;
    }

  if (self->registered)
    {
      g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (self->service));
      self->registered = FALSE;
    }

  g_clear_object (&self->service);
  g_clear_object (&self->conn);

  /* Init tasks hold a reference on @self */
  g_assert (self->init_tasks == NULL);

  g_clear_error (&self->invalidated_error);

  G_OBJECT_CLASS (trp_service_navigation_guidance_progress_parent_class)
      ->dispose (object);
}

static void
trp_service_navigation_guidance_progress_class_init (TrpServiceNavigationGuidanceProgressClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = trp_service_navigation_guidance_progress_get_property;
  object_class->set_property = trp_service_navigation_guidance_progress_set_property;
  object_class->dispose = trp_service_navigation_dispose;

  /**
   * TrpServiceNavigationGuidanceProgress:connection:
   *
   * The #GDBusConnection used by the service to publish progress information.
   *
   * Since: 0.1612.0
   */
  properties[PROP_CONNECTION] = g_param_spec_object (
      "connection", "Connection", "GDBusConnection", G_TYPE_DBUS_CONNECTION,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * TrpServiceNavigationGuidanceProgress::invalidated:
   * @self: a #TrpServiceNavigationGuidanceProgress
   * @error: error which caused @self to be invalidated
   *
   * Emitted when the backing object underlying this #TrpServiceNavigationGuidanceProgress
   * disappears, or it is otherwise disconnected.
   * The most common reason for this signal to be emitted is if the
   * underlying D-Bus service name is lost.
   *
   * After this signal is emitted, all method calls to #TrpServiceNavigationGuidanceProgress methods will
   * fail with the same error as the one reported in the signal (usually
   * #G_DBUS_ERROR_NAME_HAS_NO_OWNER).
   *
   * Since: 0.1612.0
   */
  signals[SIG_INVALIDATED] = g_signal_new ("invalidated", TRP_SERVICE_TYPE_NAVIGATION_GUIDANCE_PROGRESS,
                                           G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                           G_TYPE_NONE, 1, G_TYPE_ERROR);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

static void
trp_service_navigation_guidance_progress_init (TrpServiceNavigationGuidanceProgress *self)
{
  self->service = trp_navigation_guidance1_progress_skeleton_new ();
  self->cancellable = g_cancellable_new ();
}

/**
 * trp_service_navigation_guidance_progress_new:
 * @connection: the #GDBusConnection to use to publish routes
 *
 * Create a new #TrpServiceNavigationGuidanceProgress. This service won't expose anything
 * on D-Bus until g_async_initable_init_async() has been called.
 *
 * Returns: (transfer full): a new #TrpServiceNavigationGuidanceProgress
 * Since: 0.1612.0
 */
TrpServiceNavigationGuidanceProgress *
trp_service_navigation_guidance_progress_new (GDBusConnection *connection)
{
  g_return_val_if_fail (G_IS_DBUS_CONNECTION (connection), NULL);

  return g_object_new (TRP_SERVICE_TYPE_NAVIGATION_GUIDANCE_PROGRESS, "connection", connection, NULL);
}

static void
fail_init_tasks (TrpServiceNavigationGuidanceProgress *self,
                 GError *error)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_error (task, g_error_copy (error));
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
complete_init_tasks (TrpServiceNavigationGuidanceProgress *self)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_boolean (task, TRUE);
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
bus_name_acquired_cb (GDBusConnection *conn,
                      const gchar *name,
                      gpointer user_data)
{
  TrpServiceNavigationGuidanceProgress *self = user_data;
  GError *error = NULL;

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (self->service),
                                         conn, PROGRESS_PATH, &error))
    {
      fail_init_tasks (self, error);
    }
  else
    {
      self->registered = TRUE;
      complete_init_tasks (self);
    }
}

static void
bus_name_lost_cb (GDBusConnection *conn,
                  const gchar *name,
                  gpointer user_data)
{
  TrpServiceNavigationGuidanceProgress *self = user_data;

  g_clear_error (&self->invalidated_error);
  self->invalidated_error = g_error_new (G_DBUS_ERROR, G_DBUS_ERROR_NAME_HAS_NO_OWNER, "Lost bus name '%s'", name);

  fail_init_tasks (self, self->invalidated_error);
  g_signal_emit (self, signals[SIG_INVALIDATED], 0, self->invalidated_error);
}

static void
trp_service_navigation_guidance_progress_init_async (GAsyncInitable *initable,
                                                     int io_priority,
                                                     GCancellable *cancellable,
                                                     GAsyncReadyCallback callback,
                                                     gpointer user_data)
{
  TrpServiceNavigationGuidanceProgress *self = TRP_SERVICE_NAVIGATION_GUIDANCE_PROGRESS (initable);
  g_autoptr (GTask) task = NULL;
  gboolean start_init;

  g_return_if_fail (self->conn != NULL);

  task = g_task_new (initable, cancellable, callback, user_data);

  if (self->registered)
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  start_init = (self->init_tasks == NULL);
  self->init_tasks = g_list_append (self->init_tasks, g_object_ref (task));

  if (start_init)
    self->own_name_id = g_bus_own_name_on_connection (self->conn,
                                                      PROGRESS_BUS_NAME,
                                                      G_BUS_NAME_OWNER_FLAGS_NONE,
                                                      bus_name_acquired_cb,
                                                      bus_name_lost_cb, self,
                                                      NULL);
}

static gboolean
trp_service_navigation_guidance_progress_init_finish (GAsyncInitable *initable,
                                                      GAsyncResult *result,
                                                      GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, initable), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data)
{
  GAsyncInitableIface *iface = g_iface;

  iface->init_async = trp_service_navigation_guidance_progress_init_async;
  iface->init_finish = trp_service_navigation_guidance_progress_init_finish;
}

static gboolean
check_invalidated (TrpServiceNavigationGuidanceProgress *self,
                   GError **error)
{
  if (self->invalidated_error == NULL)
    return TRUE;

  if (error != NULL)
    *error = g_error_copy (self->invalidated_error);

  return FALSE;
}

/**
 * trp_service_navigation_guidance_progress_set_start_time:
 * @self: a #TrpServiceNavigationGuidanceProgress
 * @start_time: (nullable): a #GDateTime representing the new start time, or %NULL to unset the start time
 * @error: return location for error or %NULL.
 *
 * Set the start time of the current journey to @start_time. To unset a previous start time
 * and if there is currently no on going journey pass %NULL as @start_time.
 *
 * Returns: %TRUE if the operation succeeded, %FALSE otherwise.
 */
gboolean
trp_service_navigation_guidance_progress_set_start_time (TrpServiceNavigationGuidanceProgress *self,
                                                         GDateTime *start_time,
                                                         GError **error)
{
  guint64 ts = 0;

  g_return_val_if_fail (TRP_SERVICE_IS_NAVIGATION_GUIDANCE_PROGRESS (self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (!check_invalidated (self, error))
    return FALSE;

  if (start_time != NULL)
    ts = g_date_time_to_unix (start_time);

  trp_navigation_guidance1_progress_set_start_time (self->service, ts);
  return TRUE;
}

/**
 * trp_service_navigation_guidance_progress_set_start_time_now:
 * @self: a #TrpServiceNavigationGuidanceProgress
 * @error: return location for error or %NULL.
 *
 * Set the start time of the current journey to the current time.
 * See trp_service_navigation_guidance_progress_set_start_time() for details.
 *
 * Returns: %TRUE if the operation succeeded, %FALSE otherwise.
 */
gboolean
trp_service_navigation_guidance_progress_set_start_time_now (TrpServiceNavigationGuidanceProgress *self,
                                                             GError **error)
{
  gboolean result;
  g_autoptr (GDateTime) dt = NULL;

  g_return_val_if_fail (TRP_SERVICE_IS_NAVIGATION_GUIDANCE_PROGRESS (self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  dt = g_date_time_new_now_utc ();
  result = trp_service_navigation_guidance_progress_set_start_time (self, dt, error);

  return result;
}

/**
 * trp_service_navigation_guidance_progress_set_estimated_end_time:
 * @self: a #TrpServiceNavigationGuidanceProgress
 * @end_time: (nullable): a #GDateTime representing the new estimated end time, or %NULL to unset it
 * @error: return location for error or %NULL.
 *
 * Set the estimated time of the current journey to @end_time. To unset a previous end time
 * pass %NULL as @end_time.
 *
 * Returns: %TRUE if the operation succeeded, %FALSE otherwise.
 */
gboolean
trp_service_navigation_guidance_progress_set_estimated_end_time (TrpServiceNavigationGuidanceProgress *self,
                                                                 GDateTime *end_time,
                                                                 GError **error)
{
  guint64 ts = 0;

  g_return_val_if_fail (TRP_SERVICE_IS_NAVIGATION_GUIDANCE_PROGRESS (self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (!check_invalidated (self, error))
    return FALSE;

  if (end_time != NULL)
    ts = g_date_time_to_unix (end_time);

  trp_navigation_guidance1_progress_set_estimated_end_time (self->service, ts);
  return TRUE;
}
