/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>

#include "dbus/org.apertis.Navigation1.h"
#include "test-helper.h"
#include "traprain-client/navigation.h"
#include "traprain-service/navigation.h"

typedef struct
{
  GTestDBus *dbus;
  GDBusConnection *conn; /* owned */
  GMainLoop *loop;
  GError *error;

  TrpServiceNavigation *service;
  TrpClientNavigation *client;

  gboolean init_result;
} Test;

static void
setup (Test *test, gconstpointer unused)
{
  const gchar *addr;

  test->error = NULL;

  test->dbus = g_test_dbus_new (G_TEST_DBUS_NONE);
  g_test_dbus_up (test->dbus);
  addr = g_test_dbus_get_bus_address (test->dbus);
  g_assert (addr != NULL);

  test->conn = g_dbus_connection_new_for_address_sync (addr,
                                                       G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT |
                                                           G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION,
                                                       NULL, NULL, &test->error);
  g_assert_no_error (test->error);

  test->service = trp_service_navigation_new (test->conn);
  g_assert (TRP_SERVICE_IS_NAVIGATION (test->service));

  test->client = trp_client_navigation_new (test->conn);
  g_assert (TRP_CLIENT_IS_NAVIGATION (test->client));

  test->loop = g_main_loop_new (NULL, FALSE);
}

static void
teardown (Test *test, gconstpointer unused)
{
  g_clear_pointer (&test->loop, g_main_loop_unref);
  g_clear_pointer (&test->error, g_error_free);

  g_clear_object (&test->service);
  g_clear_object (&test->client);

  g_clear_object (&test->conn);
  g_test_dbus_down (test->dbus);
  g_clear_object (&test->dbus);
}

static void
init_cb (GObject *source,
         GAsyncResult *res,
         gpointer user_data)
{
  Test *test = user_data;

  test->init_result = g_async_initable_init_finish (G_ASYNC_INITABLE (source), res, &test->error);
  g_main_loop_quit (test->loop);
}

static void
register_service (Test *test)
{
  g_async_initable_init_async (G_ASYNC_INITABLE (test->service), G_PRIORITY_DEFAULT, NULL, init_cb, test);
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
}

static void
notify_cb (GObject *object,
           GParamSpec *spec,
           Test *test)
{
  g_main_loop_quit (test->loop);
}

static void
test_routes (Test *test,
             gconstpointer unused)
{
  GPtrArray *routes;
  TrpClientRoute *first_route, *second_route, *third_route;
  TrpClientRoute *current;
  gulong sig;

  trp_service_navigation_add_route (test->service, create_first_service_route (test->service), -1, NULL);
  trp_service_navigation_add_route (test->service, create_second_service_route (test->service), -1, NULL);
  trp_service_navigation_set_current_route (test->service, 1, NULL);

  register_service (test);

  g_async_initable_init_async (G_ASYNC_INITABLE (test->client), G_PRIORITY_DEFAULT, NULL, init_cb, test);
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert (test->init_result);

  /* Check published routes */
  routes = trp_client_navigation_get_routes (test->client);
  g_assert_cmpuint (routes->len, ==, 2);

  first_route = g_ptr_array_index (routes, 0);
  check_first_client_route (first_route);
  second_route = g_ptr_array_index (routes, 1);
  check_second_client_route (second_route);

  /* Same routes are published using the property getter */
  g_object_get (test->client, "routes", &routes, NULL);
  g_assert_cmpuint (routes->len, ==, 2);
  g_assert (g_ptr_array_index (routes, 0) == first_route);
  g_assert (g_ptr_array_index (routes, 1) == second_route);
  g_ptr_array_unref (routes);

  g_assert (trp_client_navigation_get_current_route (test->client) == second_route);
  g_object_get (test->client, "current-route", &current, NULL);
  g_assert (current == second_route);
  g_object_unref (current);

  /* Add Third route to the 2nd position and check if things are updated */
  sig = g_signal_connect (test->client, "notify::routes", G_CALLBACK (notify_cb), test);
  g_assert_cmpuint (trp_service_navigation_add_route (test->service, create_third_service_route (test->service), 1, NULL), ==, 1);
  g_main_loop_run (test->loop);

  routes = trp_client_navigation_get_routes (test->client);
  g_assert_cmpuint (routes->len, ==, 3);

  /* The same route object has been re-used */
  g_assert (g_ptr_array_index (routes, 0) == first_route);
  third_route = g_ptr_array_index (routes, 1);
  check_third_client_route (third_route);
  g_assert (g_ptr_array_index (routes, 2) == second_route);

  /* Remove first route */
  g_object_add_weak_pointer (G_OBJECT (first_route), (gpointer *) &first_route);
  trp_service_navigation_remove_route (test->service, 0, NULL);
  g_main_loop_run (test->loop);

  routes = trp_client_navigation_get_routes (test->client);
  g_assert_cmpuint (routes->len, ==, 2);
  g_assert (g_ptr_array_index (routes, 0) == third_route);
  g_assert (g_ptr_array_index (routes, 1) == second_route);
  /* First route object has been destroyed */
  g_assert (first_route == NULL);

  /* Change current route */
  g_signal_handler_disconnect (test->client, sig);
  g_signal_connect (test->client, "notify::current-route", G_CALLBACK (notify_cb), test);
  trp_service_navigation_set_current_route (test->service, 0, NULL);
  g_main_loop_run (test->loop);

  g_assert (trp_client_navigation_get_current_route (test->client) == third_route);
}

/* No current route is exposed by the service */
static void
test_no_current_route (Test *test,
                       gconstpointer unused)
{
  GPtrArray *routes;

  register_service (test);

  g_async_initable_init_async (G_ASYNC_INITABLE (test->client), G_PRIORITY_DEFAULT, NULL, init_cb, test);
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert (test->init_result);

  routes = trp_client_navigation_get_routes (test->client);
  g_assert_cmpuint (routes->len, ==, 0);
  g_assert (trp_client_navigation_get_current_route (test->client) == NULL);
}

static void
test_no_service (Test *test,
                 gconstpointer unused)
{
  g_async_initable_init_async (G_ASYNC_INITABLE (test->client), G_PRIORITY_DEFAULT, NULL, init_cb, test);
  g_main_loop_run (test->loop);
  g_assert_error (test->error, G_DBUS_ERROR, G_DBUS_ERROR_SERVICE_UNKNOWN);
  g_assert (!test->init_result);
}

int
main (int argc, char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/client/navigation/routes", Test, NULL,
              setup, test_routes, teardown);
  g_test_add ("/client/navigation/no-current-route", Test, NULL,
              setup, test_no_current_route, teardown);
  g_test_add ("/client/navigation/no-service", Test, NULL,
              setup, test_no_service, teardown);

  return g_test_run ();
}
