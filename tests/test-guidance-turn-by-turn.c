/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gio/gio.h>
#include <glib.h>

#include "traprain-guidance/turn-by-turn-service.h"
#include "traprain-service/turn-by-turn-notification.h"

#include "dbus/org.apertis.NavigationGuidance1.TurnByTurn.h"

#define TBT_BUS_NAME "org.apertis.NavigationGuidance1.TurnByTurn"
#define TBT_PATH "/org/apertis/NavigationGuidance1/TurnByTurn"

typedef struct
{
  GTestDBus *dbus;       /* owned */
  GDBusConnection *conn; /* owned */
  GMainLoop *loop;       /* owned */
  GError *error;         /* owned */
  gboolean result;
  guint wait;
  gboolean new_notification_cb_expected;

  TrpGuidanceTurnByTurnService *service;   /* owned */
  TrpNavigationGuidance1TurnByTurn *proxy; /* owned */

  TrpGuidanceTurnByTurnNotification *notif; /* owned */
} Test;

static void
setup (Test *test,
       gconstpointer unused)
{
  const gchar *addr;

  test->dbus = g_test_dbus_new (G_TEST_DBUS_NONE);
  g_test_dbus_up (test->dbus);

  addr = g_test_dbus_get_bus_address (test->dbus);
  g_assert (addr != NULL);

  test->conn = g_dbus_connection_new_for_address_sync (addr,
                                                       G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT |
                                                           G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION,
                                                       NULL, NULL, &test->error);

  test->service = trp_guidance_turn_by_turn_service_new (test->conn);
  g_assert_true (TRP_GUIDANCE_IS_TURN_BY_TURN_SERVICE (test->service));

  test->loop = g_main_loop_new (NULL, FALSE);
  test->error = NULL;
}

static void
teardown (Test *test,
          gconstpointer unused)
{
  g_clear_pointer (&test->loop, g_main_loop_unref);
  g_clear_pointer (&test->error, g_error_free);

  g_clear_object (&test->notif);
  g_clear_object (&test->service);
  g_clear_object (&test->proxy);

  g_test_dbus_down (test->dbus);
  g_clear_object (&test->dbus);
  g_clear_object (&test->conn);
}

static void
init_cb (GObject *source,
         GAsyncResult *res,
         gpointer user_data)
{
  Test *test = user_data;

  test->result = g_async_initable_init_finish (G_ASYNC_INITABLE (source), res, &test->error);
  g_main_loop_quit (test->loop);
}

static void
init_service (Test *test)
{
  g_async_initable_init_async (G_ASYNC_INITABLE (test->service), G_PRIORITY_DEFAULT, NULL, init_cb, test);
  g_main_loop_run (test->loop);
  g_assert_true (test->result);
  g_assert_no_error (test->error);
}

static void
proxy_cb (GObject *source,
          GAsyncResult *res,
          gpointer user_data)
{
  Test *test = user_data;

  test->proxy = trp_navigation_guidance1_turn_by_turn_proxy_new_finish (res, &test->error);

  g_main_loop_quit (test->loop);
}

static void
create_proxy (Test *test)
{
  g_clear_object (&test->proxy);

  trp_navigation_guidance1_turn_by_turn_proxy_new (test->conn, G_DBUS_PROXY_FLAGS_DO_NOT_AUTO_START,
                                                   TBT_BUS_NAME, TBT_PATH, NULL, proxy_cb, test);

  g_main_loop_run (test->loop);
}

/* Init a TrpGuidanceTurnByTurnService */
static void
test_service_init (Test *test,
                   gconstpointer unused)
{
  g_autofree gchar *owner = NULL;

  /* Service is not yet initialized, so not yet exposed on the bus */
  create_proxy (test);
  g_assert_no_error (test->error);
  owner = g_dbus_proxy_get_name_owner (G_DBUS_PROXY (test->proxy));
  g_assert (owner == NULL);

  init_service (test);

  /* Check that the proxy is owned now that the service is initialized */
  create_proxy (test);
  g_assert_no_error (test->error);
  owner = g_dbus_proxy_get_name_owner (G_DBUS_PROXY (test->proxy));
  g_assert (owner != NULL);
}

static void
invalidated_cb (TrpGuidanceTurnByTurnService *service,
                const GError *error,
                Test *test)
{
  g_assert (error != NULL);

  g_clear_error (&test->error);
  test->error = g_error_copy (error);
  g_main_loop_quit (test->loop);
}

/* Invalidate a TrpGuidanceTurnByTurnService */
static void
test_service_invalidated (Test *test,
                          gconstpointer unused)
{
  init_service (test);

  g_assert_no_error (test->error);
  g_signal_connect (test->service, "invalidated", G_CALLBACK (invalidated_cb),
                    test);

  g_dbus_connection_close (test->conn, NULL, NULL, NULL);
  g_main_loop_run (test->loop);

  g_assert_error (test->error, G_DBUS_ERROR, G_DBUS_ERROR_NAME_HAS_NO_OWNER);
}

static void
send_cb (GObject *source,
         GAsyncResult *result,
         gpointer user_data)
{
  TrpServiceTurnByTurnNotification *notif = TRP_SERVICE_TURN_BY_TURN_NOTIFICATION (source);
  Test *test = user_data;

  g_clear_error (&test->error);
  test->result = trp_service_turn_by_turn_notification_send_finish (notif, result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

/* Create a notification and try sending it despite no service running */
static void
test_client_create_notification (Test *test,
                                 gconstpointer unused)
{
  g_autoptr (TrpServiceTurnByTurnNotification) notif = NULL;

  notif = trp_service_turn_by_turn_notification_new (test->conn, "my summary", "my body", "my icon");
  g_assert (TRP_SERVICE_IS_TURN_BY_TURN_NOTIFICATION (notif));

  test->wait = 1;
  trp_service_turn_by_turn_notification_send_async (notif, -1, send_cb, test);
  g_main_loop_run (test->loop);

  g_assert (!test->result);
  g_assert_error (test->error, G_DBUS_ERROR, G_DBUS_ERROR_SERVICE_UNKNOWN);
}

static void
new_notification_cb (TrpGuidanceTurnByTurnService *service,
                     TrpGuidanceTurnByTurnNotification *notif,
                     Test *test)
{
  g_set_object (&test->notif, notif);

  g_assert (test->new_notification_cb_expected);
  test->wait--;

  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

static void
notify_cb (GObject *object,
           GParamSpec *spec,
           Test *test)
{
  test->wait--;

  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

static void
updated_cb (TrpGuidanceTurnByTurnNotification *notification,
            Test *test)
{
  test->wait--;

  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

static void
close_cb (GObject *source,
          GAsyncResult *result,
          gpointer user_data)
{
  TrpServiceTurnByTurnNotification *notif = TRP_SERVICE_TURN_BY_TURN_NOTIFICATION (source);
  Test *test = user_data;

  g_clear_error (&test->error);
  test->result = trp_service_turn_by_turn_notification_close_finish (notif, result, &test->error);

  test->wait--;
  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

static void
close_sig_cb (TrpGuidanceTurnByTurnNotification *notif,
              Test *test)
{
  test->wait--;

  if (test->wait == 0)
    g_main_loop_quit (test->loop);
}

/* Send, update and close a notification */
static void
test_service_notifications (Test *test,
                            gconstpointer unused)
{
  g_autoptr (TrpServiceTurnByTurnNotification) notif = NULL;

  init_service (test);

  /* Send a notification */
  notif = trp_service_turn_by_turn_notification_new (test->conn, "my summary", "my body", "my icon");
  g_assert (TRP_SERVICE_IS_TURN_BY_TURN_NOTIFICATION (notif));

  test->new_notification_cb_expected = TRUE;
  g_signal_connect (test->service, "new-notification",
                    G_CALLBACK (new_notification_cb), test);

  test->wait = 2; /* new_notification_cb and send_cb */
  trp_service_turn_by_turn_notification_send_async (notif, -1, send_cb, test);
  g_main_loop_run (test->loop);

  g_assert (test->result);
  g_assert_no_error (test->error);
  g_assert (TRP_GUIDANCE_IS_TURN_BY_TURN_NOTIFICATION (test->notif));

  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_summary (test->notif), ==, "my summary");
  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_body (test->notif), ==, "my body");
  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_icon (test->notif), ==, "my icon");
  g_assert_cmpint (trp_guidance_turn_by_turn_notification_get_expire_timeout (test->notif), ==, -1);

  /* Update the notification */
  trp_service_turn_by_turn_notification_set_summary (notif, "new summary");
  trp_service_turn_by_turn_notification_set_body (notif, "new body");
  trp_service_turn_by_turn_notification_set_icon (notif, "new icon");

  g_signal_connect (test->notif, "notify", G_CALLBACK (notify_cb), test);
  g_signal_connect (test->notif, "updated", G_CALLBACK (updated_cb), test);
  test->new_notification_cb_expected = FALSE;
  test->wait = 6; /* send_cb + notify_cb on the 4 properties + updated_cb */
  trp_service_turn_by_turn_notification_send_async (notif, 10, send_cb, test);
  g_main_loop_run (test->loop);

  g_assert (test->result);
  g_assert_no_error (test->error);

  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_summary (test->notif), ==, "new summary");
  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_body (test->notif), ==, "new body");
  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_icon (test->notif), ==, "new icon");
  g_assert_cmpint (trp_guidance_turn_by_turn_notification_get_expire_timeout (test->notif), ==, 10);

  /* Close the notification */
  g_signal_connect (test->notif, "close", G_CALLBACK (close_sig_cb), test);

  test->wait = 2; /* close_cb and close_sig_cb */
  trp_service_turn_by_turn_notification_close_async (notif, close_cb, test);
  g_main_loop_run (test->loop);

  g_assert (test->result);
  g_assert_no_error (test->error);
}

/* Try updating and closing a notification already destroyed by the server */
static void
test_service_notification_destroyed (Test *test,
                                     gconstpointer unused)
{
  g_autoptr (TrpServiceTurnByTurnNotification) notif = NULL;

  init_service (test);

  /* Send a notification */
  notif = trp_service_turn_by_turn_notification_new (test->conn, "my summary", "my body", "my icon");
  g_assert (TRP_SERVICE_IS_TURN_BY_TURN_NOTIFICATION (notif));

  test->new_notification_cb_expected = TRUE;
  g_signal_connect (test->service, "new-notification",
                    G_CALLBACK (new_notification_cb), test);

  test->wait = 2; /* new_notification_cb and send_cb */
  trp_service_turn_by_turn_notification_send_async (notif, -1, send_cb, test);
  g_main_loop_run (test->loop);

  g_assert (test->result);
  g_assert_no_error (test->error);
  g_assert (TRP_GUIDANCE_IS_TURN_BY_TURN_NOTIFICATION (test->notif));

  /* Server gets rid of the notification */
  g_clear_object (&test->notif);

  /* Try updating the notification, a new one is created */
  trp_service_turn_by_turn_notification_set_summary (notif, "new summary");

  test->wait = 2; /* new_notification_cb and send_cb */
  trp_service_turn_by_turn_notification_send_async (notif, 10, send_cb, test);
  g_main_loop_run (test->loop);

  g_assert (test->result);
  g_assert_no_error (test->error);
  g_assert (TRP_GUIDANCE_IS_TURN_BY_TURN_NOTIFICATION (test->notif));

  /* Server get rids of the notification */
  g_clear_object (&test->notif);

  /* Try closing it and get an error */
  test->wait = 1;
  trp_service_turn_by_turn_notification_close_async (notif, close_cb, test);
  g_main_loop_run (test->loop);

  g_assert (!test->result);
  g_assert_error (test->error, G_DBUS_ERROR, G_DBUS_ERROR_INVALID_ARGS);
}

/* Send and update a notification with NULL summary and icon */
static void
test_service_notification_null_args (Test *test,
                                     gconstpointer unused)
{
  g_autoptr (TrpServiceTurnByTurnNotification) notif = NULL;

  init_service (test);

  /* Send a notification */
  notif = trp_service_turn_by_turn_notification_new (test->conn, "my summary", NULL, NULL);
  g_assert (TRP_SERVICE_IS_TURN_BY_TURN_NOTIFICATION (notif));

  test->new_notification_cb_expected = TRUE;
  g_signal_connect (test->service, "new-notification",
                    G_CALLBACK (new_notification_cb), test);

  test->wait = 2;
  trp_service_turn_by_turn_notification_send_async (notif, 10, send_cb, test);
  g_main_loop_run (test->loop);

  g_assert (test->result);
  g_assert_no_error (test->error);
  g_assert (TRP_GUIDANCE_IS_TURN_BY_TURN_NOTIFICATION (test->notif));

  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_summary (test->notif), ==, "my summary");
  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_body (test->notif), ==, NULL);
  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_icon (test->notif), ==, NULL);
  g_assert_cmpint (trp_guidance_turn_by_turn_notification_get_expire_timeout (test->notif), ==, 10);

  /* Set a body and icon */
  trp_service_turn_by_turn_notification_set_summary (notif, "new summary");
  trp_service_turn_by_turn_notification_set_body (notif, "new body");
  trp_service_turn_by_turn_notification_set_icon (notif, "new icon");

  g_signal_connect (test->notif, "notify", G_CALLBACK (notify_cb), test);
  test->new_notification_cb_expected = FALSE;
  test->wait = 5; /* send_cb + notify_cb on the 4 properties */
  trp_service_turn_by_turn_notification_send_async (notif, 0, send_cb, test);
  g_main_loop_run (test->loop);

  g_assert (test->result);
  g_assert_no_error (test->error);

  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_summary (test->notif), ==, "new summary");
  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_body (test->notif), ==, "new body");
  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_icon (test->notif), ==, "new icon");
  g_assert_cmpint (trp_guidance_turn_by_turn_notification_get_expire_timeout (test->notif), ==, 0);

  /* Unset a body and icon */
  trp_service_turn_by_turn_notification_set_summary (notif, "super new summary");
  trp_service_turn_by_turn_notification_set_body (notif, NULL);
  trp_service_turn_by_turn_notification_set_icon (notif, NULL);

  test->new_notification_cb_expected = FALSE;
  test->wait = 5; /* send_cb + notify_cb on the 4 properties */
  trp_service_turn_by_turn_notification_send_async (notif, 100, send_cb, test);
  g_main_loop_run (test->loop);

  g_assert (test->result);
  g_assert_no_error (test->error);

  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_summary (test->notif), ==, "super new summary");
  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_body (test->notif), ==, NULL);
  g_assert_cmpstr (trp_guidance_turn_by_turn_notification_get_icon (test->notif), ==, NULL);
  g_assert_cmpint (trp_guidance_turn_by_turn_notification_get_expire_timeout (test->notif), ==, 100);
}

int
main (int argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/guidance/turn-by-turn/service/init", Test, NULL,
              setup, test_service_init, teardown);
  g_test_add ("/guidance/turn-by-turn/service/invalidated", Test, NULL,
              setup, test_service_invalidated, teardown);
  g_test_add ("/guidance/turn-by-turn/client/create-notification", Test, NULL,
              setup, test_client_create_notification, teardown);
  g_test_add ("/guidance/turn-by-turn/service/notifications", Test, NULL,
              setup, test_service_notifications, teardown);
  g_test_add ("/guidance/turn-by-turn/service/notification_destroyed", Test, NULL,
              setup, test_service_notification_destroyed, teardown);
  g_test_add ("/guidance/turn-by-turn/service/notification_null_args", Test, NULL,
              setup, test_service_notification_null_args, teardown);

  return g_test_run ();
}
