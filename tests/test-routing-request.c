/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>

#include <string.h>

#include "traprain-common/routing-request.h"

/* Abbreviate the data table a bit */
#define DESC TRP_ROUTING_PARAMETER_DESCRIPTION
#define VIA TRP_ROUTING_PARAMETER_VIA
#define WAY TRP_ROUTING_PARAMETER_WAYPOINT
#define END "--end--"
#define TWICE(x) x, x

typedef struct
{
  const char *uri;
  gint code_when_tolerant;
  gint code_when_strict;
  const char *dest;

  const char *name1;
  const char *value1;
  const char *place1;

  const char *name2;
  const char *value2;
  const char *place2;

  const char *name3;
  const char *value3;
  const char *place3;

  const char *name4;
  const char *value4;
  const char *place4;

  const char *name5;
  const char *value5;
  const char *place5;

  const char *name6;
} UriTest;

static UriTest uris[] =
    {
      /* The good: parses successfully */
      { "nav:place:London", -1, -1, "London",
        END },
      { "nav:place:London;description=to%20London;foo=bar", -1, -1, "London",
        DESC, "to London", NULL,
        "foo", "bar", NULL,
        END },
      { "nav:place:London;way=place:Brighton", -1, -1, "London",
        WAY, "place:Brighton", "Brighton",
        END },
      { "nav:place:London;way=place:Brighton;way=place:Three%2520Bridges", -1, -1,
        "London",
        WAY, "place:Brighton", "Brighton",
        WAY, "place:Three%20Bridges", "Three Bridges",
        END },
      { "nav:place:London;via=place:Brighton;via=place:Three%2520Bridges", -1, -1,
        "London",
        VIA, "place:Brighton", "Brighton",
        VIA, "place:Three%20Bridges", "Three Bridges",
        END },
      { "nav:place:London;via=place:A;foo;way=place:B;bar=place:X;via=place:C",
        -1, -1, "London",
        VIA, "place:A", "A",
        "foo", "", NULL,
        WAY, "place:B", "B",
        "bar", "place:X", "X",
        VIA, "place:C", "C",
        END },

      /* The bad: always parses unsuccessfully */
      { "hello", TWICE (TRP_PLACE_ERROR_UNKNOWN_SCHEME) },
      { "about:blank", TWICE (TRP_PLACE_ERROR_UNKNOWN_SCHEME) },
      { "geo:", TWICE (TRP_PLACE_ERROR_UNKNOWN_SCHEME) },
      { "place:", TWICE (TRP_PLACE_ERROR_UNKNOWN_SCHEME) },
      { "nav://", TWICE (TRP_PLACE_ERROR_INVALID_URI) },
      { "nav:Wales", TWICE (TRP_PLACE_ERROR_INVALID_URI) },
      { "nav:place:Wales?", TWICE (TRP_PLACE_ERROR_INVALID_URI) },
      { "nav:place:A;via=;way=place:B", TWICE (TRP_PLACE_ERROR_INVALID_URI) },
      { "nav:place:A;way;via=place:B", TWICE (TRP_PLACE_ERROR_INVALID_URI) },
      { "nav:geo:0%2C1%3Bcrs=",
        TRP_PLACE_ERROR_UNKNOWN_CRS, TRP_PLACE_ERROR_INVALID_URI },

      /* The ugly: only parses in tolerant mode */
      { "nav:place:London%3Bbuilding=", -1, TRP_PLACE_ERROR_INVALID_URI,
        "London",
        END },
      { "nav:place:London;description", -1, TRP_PLACE_ERROR_INVALID_URI,
        "London",
        END },
      { "nav:place:London;description=", -1, TRP_PLACE_ERROR_INVALID_URI,
        "London",
        END },
    };

static void
test_one (const UriTest *uri,
          TrpUriFlags flags)
{
  g_autoptr (GError) error = NULL;
  g_autoptr (TrpRoutingRequest) request =
      trp_routing_request_new_for_uri (uri->uri, flags, &error);
  g_autoptr (TrpPlace) destination = NULL;
  g_autoptr (TrpPlace) destination2 = NULL;
  g_autoptr (GPtrArray) parameters = NULL;
  g_autoptr (GPtrArray) parameters2 = NULL;
  struct
  {
    const char *name;
    const char *value;
    const char *place;
  } route[] =
      {
        { uri->name1, uri->value1, uri->place1 },
        { uri->name2, uri->value2, uri->place2 },
        { uri->name3, uri->value3, uri->place3 },
        { uri->name4, uri->value4, uri->place4 },
        { uri->name5, uri->value5, uri->place5 },
        { uri->name6 },
      };
  gsize i, j;

  g_test_message ("%s", uri->uri);

  if ((flags & TRP_URI_FLAGS_STRICT) && uri->code_when_strict >= 0)
    {
      g_assert_error (error, TRP_PLACE_ERROR, uri->code_when_strict);
      g_test_message ("-> %s", error->message);
      return;
    }

  if (!(flags & TRP_URI_FLAGS_STRICT) && uri->code_when_tolerant >= 0)
    {
      g_assert_error (error, TRP_PLACE_ERROR, uri->code_when_tolerant);
      g_test_message ("-> %s", error->message);
      return;
    }

  g_assert_no_error (error);
  g_assert_nonnull (request);

  g_object_get (request,
                "destination", &destination,
                "parameters", &parameters,
                NULL);

  g_assert_cmpstr (trp_place_get_location_string (destination), ==,
                   uri->dest);
  destination2 = trp_routing_request_get_destination (request);
  parameters2 = trp_routing_request_get_parameters (request);
  g_assert_true (destination == destination2);
  /* We copy the array to avoid callers being able to modify it */
  g_assert_true (parameters != parameters2);

  for (i = 0; i < G_N_ELEMENTS (route); i++)
    {
      TrpRoutingParameter *parameter;
      TrpRoutingParameter *parameter2;
      g_autoptr (TrpPlace) place = NULL;
      g_autoptr (TrpPlace) place2 = NULL;

      /* If this assertion fails, the END is missing in the data table */
      g_assert (route[i].name != NULL);

      if (strcmp (route[i].name, END) == 0)
        {
          g_assert_cmpuint (i, ==, parameters->len);
          g_assert_cmpuint (i, ==, parameters2->len);
          break;
        }

      parameter = g_ptr_array_index (parameters, i);
      parameter2 = g_ptr_array_index (parameters2, i);

      g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==,
                       trp_routing_parameter_get_name (parameter2));
      g_assert_cmpstr (trp_routing_parameter_get_value (parameter), ==,
                       trp_routing_parameter_get_value (parameter2));

      place = trp_routing_parameter_get_place (parameter);
      place2 = trp_routing_parameter_get_place (parameter2);
      g_assert_true (place == place2);
      g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==,
                       route[i].name);
      g_assert_cmpstr (trp_routing_parameter_get_value (parameter), ==,
                       route[i].value);

      if (place == NULL)
        g_assert_cmpstr (route[i].place, ==, NULL);
      else
        g_assert_cmpstr (trp_place_get_location_string (place), ==,
                         route[i].place);
    }

  g_clear_pointer (&parameters, g_ptr_array_unref);
  parameters = trp_routing_request_get_points (request);

  for (i = 0, j = 0; i < G_N_ELEMENTS (route); i++)
    {
      TrpRoutingParameter *point;
      g_autoptr (TrpPlace) place = NULL;

      point = g_ptr_array_index (parameters, j);
      place = trp_routing_parameter_get_place (point);

      if (strcmp (route[i].name, END) == 0)
        {
          g_assert_cmpuint (j, ==, parameters->len - 1);

          g_assert_cmpstr (trp_routing_parameter_get_name (point), ==, WAY);
          g_assert_cmpstr (trp_place_get_location_string (place), ==,
                           uri->dest);

          break;
        }
      else if (strcmp (route[i].name, WAY) == 0 ||
               strcmp (route[i].name, VIA) == 0)
        {
          g_assert_cmpstr (trp_routing_parameter_get_name (point), ==,
                           route[i].name);
          g_assert_cmpstr (trp_routing_parameter_get_value (point), ==,
                           route[i].value);

          if (place == NULL)
            g_assert_cmpstr (route[i].place, ==, NULL);
          else
            g_assert_cmpstr (trp_place_get_location_string (place), ==,
                             route[i].place);

          j++;
        }
      /* else ignore a non-waypoint, non-via parameter which we do not
       * expect to see in the list of points */
    }
}

static void
test_from_uri (void)
{
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (uris); i++)
    {
      test_one (uris + i, TRP_URI_FLAGS_NONE);
      test_one (uris + i, TRP_URI_FLAGS_STRICT);
    }
}

static void
test_builder (void)
{
  g_autoptr (TrpRoutingRequest) request = NULL;
  g_autoptr (TrpPlace) place = NULL;
  g_autoptr (GPtrArray) parameters = NULL;
  TrpRoutingParameter *parameter;
  g_autoptr (TrpPlace) london =
      trp_place_new_for_uri ("place:London", TRP_URI_FLAGS_NONE, NULL);
  g_autoptr (TrpPlace) cambridge =
      trp_place_new_for_uri ("place:Cambridge", TRP_URI_FLAGS_NONE, NULL);
  g_autoptr (TrpPlace) ely =
      trp_place_new_for_uri ("place:Ely", TRP_URI_FLAGS_NONE, NULL);
  g_autoptr (TrpPlace) norwich =
      trp_place_new_for_uri ("place:Norwich", TRP_URI_FLAGS_NONE, NULL);
  g_autoptr (TrpRoutingRequestBuilder) builder =
      trp_routing_request_builder_new (norwich, NULL);
  g_autofree gchar *uri = NULL;
  g_auto (GStrv) notes = NULL;
  const gchar *note;

  g_assert_true (trp_routing_request_builder_ref (builder) == builder);
  trp_routing_request_builder_unref (builder);

  trp_routing_request_builder_set_destination (builder, norwich);

  request = trp_routing_request_builder_copy (builder);
  uri = trp_routing_request_to_uri (request, TRP_URI_SCHEME_ANY);
  g_assert_cmpstr (uri, ==, "nav:place:Norwich");
  g_clear_pointer (&uri, g_free);
  place = trp_routing_request_get_destination (request);
  g_assert_true (place == norwich);
  g_clear_object (&place);

  parameters = trp_routing_request_get_parameters (request);
  g_assert_cmpint (parameters->len, ==, 0);
  g_clear_pointer (&parameters, g_ptr_array_unref);

  parameters = trp_routing_request_get_points (request);
  g_assert_cmpint (parameters->len, ==, 1);
  parameter = g_ptr_array_index (parameters, 0);
  g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==, WAY);
  place = trp_routing_parameter_get_place (parameter);
  g_assert_true (place == norwich);
  g_clear_object (&place);
  g_clear_pointer (&parameters, g_ptr_array_unref);

  g_clear_object (&request);

  trp_routing_request_builder_set_description (builder, "My route");
  trp_routing_request_builder_add_waypoint (builder, london);
  trp_routing_request_builder_add_parameter (builder, "x-my-note",
                                             "probably the M11");
  trp_routing_request_builder_add_via (builder, cambridge);
  trp_routing_request_builder_add_via (builder, ely);
  trp_routing_request_builder_add_parameter (builder, "X-My-Note",
                                             "slow down");
  /* A final waypoint that is the same as the destination is not special;
   * it would just appear twice in get_points(). */
  trp_routing_request_builder_add_waypoint (builder, norwich);

  request = trp_routing_request_builder_copy (builder);
  uri = trp_routing_request_to_uri (request, TRP_URI_SCHEME_NAV);
  g_assert_cmpstr (uri, ==, "nav:place:Norwich"
                            ";description=My%20route"
                            ";way=place:London"
                            ";x-my-note=probably%20the%20M11"
                            ";via=place:Cambridge"
                            ";via=place:Ely"
                            ";x-my-note=slow%20down"
                            ";way=place:Norwich");
  g_clear_pointer (&uri, g_free);
  g_assert_cmpstr (trp_routing_request_get_description (request), ==,
                   "My route");
  place = trp_routing_request_get_destination (request);
  g_assert_true (place == norwich);
  g_clear_object (&place);

  parameters = trp_routing_request_get_points (request);
  g_assert_cmpint (parameters->len, ==, 5);

  parameter = g_ptr_array_index (parameters, 0);
  g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==, WAY);
  place = trp_routing_parameter_get_place (parameter);
  g_assert_true (place == london);
  g_clear_object (&place);

  parameter = g_ptr_array_index (parameters, 1);
  g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==, VIA);
  place = trp_routing_parameter_get_place (parameter);
  g_assert_true (place == cambridge);
  g_clear_object (&place);

  parameter = g_ptr_array_index (parameters, 2);
  g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==, VIA);
  place = trp_routing_parameter_get_place (parameter);
  g_assert_true (place == ely);
  g_clear_object (&place);

  parameter = g_ptr_array_index (parameters, 3);
  g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==, WAY);
  place = trp_routing_parameter_get_place (parameter);
  g_assert_true (place == norwich);
  g_clear_object (&place);

  parameter = g_ptr_array_index (parameters, 4);
  g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==, WAY);
  place = trp_routing_parameter_get_place (parameter);
  g_assert_true (place == norwich);
  g_clear_object (&place);

  g_clear_pointer (&parameters, g_ptr_array_unref);

  parameters = trp_routing_request_get_parameters (request);
  g_assert_cmpint (parameters->len, ==, 7);

  parameter = g_ptr_array_index (parameters, 0);
  g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==, DESC);
  g_assert_cmpstr (trp_routing_parameter_get_value (parameter), ==, "My route");
  place = trp_routing_parameter_get_place (parameter);
  g_assert_true (place == NULL);

  parameter = g_ptr_array_index (parameters, 1);
  g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==, WAY);
  place = trp_routing_parameter_get_place (parameter);
  g_assert_true (place == london);
  g_clear_object (&place);

  parameter = g_ptr_array_index (parameters, 2);
  g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==, "x-my-note");
  g_assert_cmpstr (trp_routing_parameter_get_value (parameter), ==,
                   "probably the M11");

  parameter = g_ptr_array_index (parameters, 3);
  g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==, VIA);
  place = trp_routing_parameter_get_place (parameter);
  g_assert_true (place == cambridge);
  g_clear_object (&place);

  parameter = g_ptr_array_index (parameters, 4);
  g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==, VIA);
  place = trp_routing_parameter_get_place (parameter);
  g_assert_true (place == ely);
  g_clear_object (&place);

  parameter = g_ptr_array_index (parameters, 5);
  g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==, "x-my-note");
  g_assert_cmpstr (trp_routing_parameter_get_value (parameter), ==,
                   "slow down");

  parameter = g_ptr_array_index (parameters, 6);
  g_assert_cmpstr (trp_routing_parameter_get_name (parameter), ==, WAY);
  place = trp_routing_parameter_get_place (parameter);
  g_assert_true (place == norwich);
  g_clear_object (&place);

  g_clear_pointer (&parameters, g_ptr_array_unref);

  notes = trp_routing_request_get_parameter_values (request, "X-MY-NOTE");
  g_assert_nonnull (notes);
  g_assert_cmpstr (notes[0], ==, "probably the M11");
  g_assert_cmpstr (notes[1], ==, "slow down");
  g_assert_cmpstr (notes[2], ==, NULL);
  g_clear_pointer (&notes, g_strfreev);

  note = trp_routing_request_get_parameter_value (request, "x-my-note");
  /* non-unique, so no well-defined value */
  g_assert_cmpstr (note, ==, NULL);

  g_clear_object (&request);

  trp_routing_request_builder_set_parameter (builder, "X-My-Note", "hello");
  request = trp_routing_request_builder_end (builder);

  note = trp_routing_request_get_parameter_value (request, "x-my-note");
  /* now there is a unique value */
  g_assert_cmpstr (note, ==, "hello");

  notes = trp_routing_request_get_parameter_values (request, "X-MY-NOTE");
  g_assert_nonnull (notes);
  g_assert_cmpstr (notes[0], ==, "hello");
  g_assert_cmpstr (notes[1], ==, NULL);
  g_clear_pointer (&notes, g_strfreev);

  note = trp_routing_request_get_parameter_value (request, "foo");
  g_assert_cmpstr (note, ==, NULL);

  g_clear_object (&request);

  trp_routing_request_builder_set_destination (builder, london);
  request = trp_routing_request_builder_copy (builder);
  uri = trp_routing_request_to_uri (request, TRP_URI_SCHEME_NAV);
  g_assert_cmpstr (uri, ==, "nav:place:London");
  g_clear_pointer (&uri, g_free);
  g_clear_object (&request);

  trp_routing_request_builder_set_description (builder, "");
  request = trp_routing_request_builder_copy (builder);
  uri = trp_routing_request_to_uri (request, TRP_URI_SCHEME_NAV);
  g_assert_cmpstr (uri, ==, "nav:place:London");
  g_clear_pointer (&uri, g_free);
  g_clear_object (&request);

  g_clear_pointer (&builder, trp_routing_request_builder_unref);
  builder = trp_routing_request_builder_new (ely, "Route");
  request = trp_routing_request_builder_end (builder);
  uri = trp_routing_request_to_uri (request, TRP_URI_SCHEME_NAV);
  g_assert_cmpstr (uri, ==, "nav:place:Ely;description=Route");
  g_clear_pointer (&uri, g_free);
  g_clear_object (&request);

  g_clear_pointer (&builder, trp_routing_request_builder_unref);
  builder = trp_routing_request_builder_new (ely, "");
  request = trp_routing_request_builder_end (builder);
  uri = trp_routing_request_to_uri (request, TRP_URI_SCHEME_NAV);
  g_assert_cmpstr (uri, ==, "nav:place:Ely");
  g_clear_pointer (&uri, g_free);
  g_clear_object (&request);

  g_clear_pointer (&builder, trp_routing_request_builder_unref);
  builder = trp_routing_request_builder_new (ely, NULL);
  request = trp_routing_request_builder_end (builder);
  uri = trp_routing_request_to_uri (request, TRP_URI_SCHEME_NAV);
  g_assert_cmpstr (uri, ==, "nav:place:Ely");
  g_clear_pointer (&uri, g_free);
  g_clear_object (&request);
}

static void
test_constructor (void)
{
  g_autoptr (TrpRoutingRequest) request = NULL;
  g_autofree gchar *moscow_encoded =
      g_uri_escape_string ("Москва́", NULL, FALSE);
  g_autofree gchar *moscow_encoded_twice =
      g_uri_escape_string (moscow_encoded, NULL, FALSE);
  g_autoptr (TrpPlace) moscow =
      trp_place_new_for_uri ("place:Москва́", TRP_URI_FLAGS_NONE, NULL);
  g_autofree gchar *uri = NULL;
  g_autofree gchar *expected = NULL;

  request = trp_routing_request_new (moscow, "Visit Moscow");
  uri = trp_routing_request_to_uri (request, TRP_URI_SCHEME_NAV);
  expected = g_strconcat ("nav:place:", moscow_encoded_twice,
                          ";description=Visit%20Moscow", NULL);
  g_assert_cmpstr (uri, ==, expected);
  g_clear_pointer (&uri, g_free);
  g_clear_pointer (&expected, g_free);
  g_clear_object (&request);

  request = trp_routing_request_new (moscow, "");
  uri = trp_routing_request_to_uri (request, TRP_URI_SCHEME_ANY);
  expected = g_strconcat ("nav:place:", moscow_encoded_twice, NULL);
  g_assert_cmpstr (uri, ==, expected);
  g_clear_pointer (&uri, g_free);
  g_clear_pointer (&expected, g_free);
  g_clear_object (&request);

  request = trp_routing_request_new (moscow, NULL);
  uri = trp_routing_request_to_uri (request, TRP_URI_SCHEME_NAV);
  expected = g_strconcat ("nav:place:", moscow_encoded_twice, NULL);
  g_assert_cmpstr (uri, ==, expected);
  g_clear_pointer (&uri, g_free);
  g_clear_pointer (&expected, g_free);
  g_clear_object (&request);
}

static void
test_preferences (void)
{
  const gchar *s;
  gboolean b;
  g_autofree gchar *uri = NULL;
  g_autoptr (TrpRoutingRequestBuilder) builder = NULL;
  g_autoptr (TrpRoutingRequest) request = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (TrpPlace) home =
      trp_place_new_for_uri ("place:Home", TRP_URI_FLAGS_NONE, NULL);

  builder = trp_routing_request_builder_new (home, "Preference test builder");
  trp_routing_request_builder_set_parameter_boolean (builder, "use-bus-lane", TRUE);
  trp_routing_request_builder_set_parameter (builder, "optimise", "shortest");

  request = trp_routing_request_builder_end (builder);

  s = trp_routing_request_get_preference (request, "optimise", "fastest");
  g_assert_cmpstr (s, ==, "shortest");

  s = trp_routing_request_get_preference (request, "nonexisting", "dummy");
  g_assert_cmpstr (s, ==, "dummy");

  b = trp_routing_request_get_preference_boolean (request, "use-bus-lane", FALSE);
  g_assert_cmpuint (b, ==, TRUE);

  uri = trp_routing_request_to_uri (request, TRP_URI_SCHEME_ANY);
  g_assert_cmpstr (uri, ==, "nav:place:Home"
                            ";description=Preference%20test%20builder"
                            ";use-bus-lane=1"
                            ";optimise=shortest"
                            "");
  g_clear_pointer (&uri, g_free);
  g_clear_object (&request);

  request = trp_routing_request_new_for_uri ("nav:place:Home"
                                             ";description=My%20Home"
                                             ";existingnamevalue=value"
                                             ";vehicle=car"
                                             ";avoid-tolls"
                                             ";avoid-bridges=True"
                                             ";avoid-narrow-roads=0"
                                             "",
                                             TRP_URI_FLAGS_NONE,
                                             &error);

  /* avoid-tolls must not be recognized as a valid boolean, testing it
   * returns default value.
   */
  b = trp_routing_request_get_preference_boolean (request, "avoid-tolls", FALSE);
  g_assert_cmpuint (b, ==, FALSE);
  b = trp_routing_request_get_preference_boolean (request, "avoid-tolls", TRUE);
  g_assert_cmpuint (b, ==, TRUE);

  b = trp_routing_request_get_preference_boolean (request, "avoid-ferries", TRUE);
  g_assert_cmpuint (b, ==, TRUE);

  b = trp_routing_request_get_preference_boolean (request, "avoid-narrow-roads", TRUE);
  g_assert_cmpuint (b, ==, FALSE);

  b = trp_routing_request_get_preference_boolean (request, "avoid-bridges", FALSE);
  g_assert_cmpuint (b, ==, TRUE);

  s = trp_routing_request_get_preference (request, "vehicle", "bike");
  g_assert_cmpstr (s, ==, "car");

  s = trp_routing_request_get_preference (request, "optimise", "fastest");
  g_assert_cmpstr (s, ==, "fastest");

  uri = trp_routing_request_to_uri (request, TRP_URI_SCHEME_ANY);
  g_assert_cmpstr (uri, ==, "nav:place:Home"
                            ";description=My%20Home"
                            ";existingnamevalue=value"
                            ";vehicle=car"
                            ";avoid-tolls"
                            ";avoid-bridges=True"
                            ";avoid-narrow-roads=0"
                            "");
}

int
main (int argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/routing-request/from-uri", test_from_uri);
  g_test_add_func ("/routing-request/builder", test_builder);
  g_test_add_func ("/routing-request/constructor", test_constructor);
  g_test_add_func ("/routing-request/preferences", test_preferences);

  return g_test_run ();
}
