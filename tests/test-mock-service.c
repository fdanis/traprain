/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <gio/gio.h>
#include <glib.h>

#include "test-helper.h"

#include "traprain-client/mock.h"
#include "traprain-client/navigation-guidance-progress.h"
#include "traprain-client/navigation.h"

typedef struct
{
  GTestDBus *dbus;
  GDBusConnection *conn; /* owned */
  guint wait_count;
  GMainLoop *loop;
  GError *error;
  /* Index returned by trp_client_mock_add_route_finish() */
  gint index;
  gboolean result;

  TrpClientNavigation *client;
  TrpClientMock *mock;
  TrpClientNavigationGuidanceProgress *client_progress;
} Test;

static void
setup (Test *test, gconstpointer unused)
{
  const gchar *addr;

  test->error = NULL;

  if (g_getenv ("TRAPRAIN_TESTS_UNINSTALLED") != NULL)
    {
      /* Test is not installed, running the mock service on the session bus for easy testing */
      test->dbus = g_test_dbus_new (G_TEST_DBUS_NONE);
      g_test_dbus_add_service_dir (test->dbus, TEST_SERVICES_DIR);
      g_test_dbus_up (test->dbus);
      addr = g_test_dbus_get_bus_address (test->dbus);
      g_assert (addr != NULL);

      test->conn = g_dbus_connection_new_for_address_sync (addr,
                                                           G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT |
                                                               G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION,
                                                           NULL, NULL, &test->error);
    }
  else
    {
      test->conn = g_bus_get_sync (G_BUS_TYPE_SYSTEM, NULL, &test->error);
    }

  g_assert_no_error (test->error);

  test->client = trp_client_navigation_new (test->conn);
  test->client_progress = trp_client_navigation_guidance_progress_new (test->conn);

  test->loop = g_main_loop_new (NULL, FALSE);
}

static void clear_routes (Test *test);

static void
teardown (Test *test, gconstpointer unused)
{
  if (test->mock != NULL)
    {
      /* clean up everything to prevent routes from staying announced if we are
       * using the real mock service (installed test). */
      clear_routes (test);
    }

  g_clear_pointer (&test->loop, g_main_loop_unref);
  g_clear_pointer (&test->error, g_error_free);

  g_clear_object (&test->client);
  g_clear_object (&test->mock);

  g_clear_object (&test->conn);
  if (test->dbus != NULL)
    {
      g_test_dbus_down (test->dbus);
      g_clear_object (&test->dbus);
    }
}

static void
client_init_cb (GObject *source,
                GAsyncResult *result,
                gpointer user_data)
{
  Test *test = user_data;

  g_async_initable_init_finish (G_ASYNC_INITABLE (source), result,
                                &test->error);
  g_main_loop_quit (test->loop);
}

static void
init_client (Test *test)
{
  g_async_initable_init_async (G_ASYNC_INITABLE (test->client),
                               G_PRIORITY_DEFAULT, NULL,
                               client_init_cb, test);
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
}

/* Test activation of the service using TrpClientNavigation */
static void
test_activation_client_service (Test *test,
                                gconstpointer unused)
{
  GPtrArray *routes;

  init_client (test);

  routes = trp_client_navigation_get_routes (test->client);
  g_assert_cmpuint (routes->len, ==, 0);
  g_assert (trp_client_navigation_get_current_route (test->client) == NULL);
}

static void
mock_new_cb (GObject *source,
             GAsyncResult *result,
             gpointer user_data)
{
  Test *test = user_data;

  test->mock = trp_client_mock_new_finish (result, &test->error);
  g_main_loop_quit (test->loop);
}

static void
clear_routes_cb (GObject *source,
                 GAsyncResult *result,
                 gpointer user_data)
{
  Test *test = user_data;

  g_clear_error (&test->error);
  test->result = trp_client_mock_clear_routes_finish (TRP_CLIENT_MOCK (source),
                                                      result, &test->error);

  test->wait_count--;
  if (test->wait_count == 0)
    g_main_loop_quit (test->loop);
}

static void
clear_routes (Test *test)
{
  test->wait_count = 1;
  trp_client_mock_clear_routes_async (test->mock, clear_routes_cb, test);
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
}

static void
init_mock (Test *test)
{
  trp_client_mock_new_async (test->conn, NULL, mock_new_cb, test);
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert (TRP_CLIENT_IS_MOCK (test->mock));

  /* When using the real system mock service (installed tests) some routes may
   * already be announced. Clean it up to make sure tests are running from a
   * clean state. */
  clear_routes (test);
}

/* Test activation of the service using TrpClientMock */
static void
test_activation_client_mock (Test *test,
                             gconstpointer unused)
{
  init_mock (test);
}

static void
add_route_cb (GObject *source,
              GAsyncResult *result,
              gpointer user_data)
{
  Test *test = user_data;

  test->index = trp_client_mock_add_route_finish (TRP_CLIENT_MOCK (source),
                                                  result, &test->error);

  test->wait_count--;
  if (test->wait_count == 0)
    g_main_loop_quit (test->loop);
}

static void
notify_cb (GObject *client,
           GParamSpec *spec,
           Test *test)
{
  test->wait_count--;
  if (test->wait_count == 0)
    g_main_loop_quit (test->loop);
}

static TrpRoute *
create_first_mock_route (Test *test)
{
  TrpRoute *route;

  route = trp_client_mock_create_route (test->mock, FST_ROUTE_DISTANCE, FST_ROUTE_TIME);
  build_first_route (route);

  return route;
}

static TrpRoute *
create_second_mock_route (Test *test)
{
  TrpRoute *route;

  route = trp_client_mock_create_route (test->mock, SND_ROUTE_DISTANCE, SND_ROUTE_TIME);
  build_second_route (route);

  return route;
}

static void
add_route (Test *test,
           TrpRoute *route)
{
  /* Wait for add_route_cb and notify_cb (needs to be connected) */
  test->wait_count = 2;
  trp_client_mock_add_route_async (test->mock, route, add_route_cb, test);
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
}

/* Test adding a route */
static void
test_add_route (Test *test,
                gconstpointer unused)
{
  GPtrArray *routes;

  init_client (test);
  init_mock (test);

  g_signal_connect (test->client, "notify::routes", G_CALLBACK (notify_cb), test);

  /* Add first route */
  add_route (test, create_first_mock_route (test));
  g_assert_cmpint (test->index, ==, 0);

  routes = trp_client_navigation_get_routes (test->client);
  g_assert_cmpuint (routes->len, ==, 1);
  check_first_client_route (g_ptr_array_index (routes, 0));

  /* Add second route */
  add_route (test, create_second_mock_route (test));
  g_assert_cmpint (test->index, ==, 1);

  routes = trp_client_navigation_get_routes (test->client);
  g_assert_cmpuint (routes->len, ==, 2);
  check_first_client_route (g_ptr_array_index (routes, 0));
  check_second_client_route (g_ptr_array_index (routes, 1));
}

static void
remove_route_cb (GObject *source,
                 GAsyncResult *result,
                 gpointer user_data)
{
  Test *test = user_data;

  test->result = trp_client_mock_remove_route_finish (TRP_CLIENT_MOCK (source),
                                                      result, &test->error);

  test->wait_count--;
  if (test->wait_count == 0)
    g_main_loop_quit (test->loop);
}

/* Try removing routes from the mock service */
static void
test_remove_route (Test *test,
                   gconstpointer unused)
{
  GPtrArray *routes;

  init_client (test);
  init_mock (test);

  g_signal_connect (test->client, "notify::routes", G_CALLBACK (notify_cb), test);

  /* Add 2 routes */
  add_route (test, create_first_mock_route (test));
  g_assert_cmpint (test->index, ==, 0);

  /* Add second route */
  add_route (test, create_second_mock_route (test));
  g_assert_cmpint (test->index, ==, 1);

  routes = trp_client_navigation_get_routes (test->client);
  g_assert_cmpuint (routes->len, ==, 2);
  check_first_client_route (g_ptr_array_index (routes, 0));
  check_second_client_route (g_ptr_array_index (routes, 1));

  /* Remove the second route */
  test->wait_count = 2;
  trp_client_mock_remove_route_async (test->mock, 1, remove_route_cb, test);
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert (test->result);

  routes = trp_client_navigation_get_routes (test->client);
  g_assert_cmpuint (routes->len, ==, 1);
  check_first_client_route (g_ptr_array_index (routes, 0));

  /* Try removing an invalid route */
  test->wait_count = 1;
  trp_client_mock_remove_route_async (test->mock, 10, remove_route_cb, test);
  g_main_loop_run (test->loop);
  g_assert_error (test->error, G_DBUS_ERROR, G_DBUS_ERROR_INVALID_ARGS);
  g_assert (!test->result);
}

static void
set_current_route_cb (GObject *source,
                      GAsyncResult *result,
                      gpointer user_data)
{
  Test *test = user_data;

  test->result = trp_client_mock_set_current_route_finish (TRP_CLIENT_MOCK (source),
                                                           result, &test->error);

  test->wait_count--;
  if (test->wait_count == 0)
    g_main_loop_quit (test->loop);
}

static void
set_current_route (Test *test,
                   guint index)
{
  /* Wait for set_current_route_cb and notify_cb (needs to be connected) */
  test->wait_count = 2;

  trp_client_mock_set_current_route_async (test->mock, index, set_current_route_cb, test);
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert (test->result);
}

/* Change current route */
static void
test_current_route (Test *test,
                    gconstpointer unused)
{
  TrpClientRoute *route;

  init_client (test);
  init_mock (test);

  g_signal_connect (test->client, "notify::routes", G_CALLBACK (notify_cb), test);

  /* Add 2 routes */
  add_route (test, create_first_mock_route (test));
  g_assert_cmpint (test->index, ==, 0);

  add_route (test, create_second_mock_route (test));
  g_assert_cmpint (test->index, ==, 1);

  route = trp_client_navigation_get_current_route (test->client);
  g_assert (route == NULL);

  /* Set the second route as current */
  g_signal_connect (test->client, "notify::current-route", G_CALLBACK (notify_cb), test);

  set_current_route (test, 1);
  route = trp_client_navigation_get_current_route (test->client);
  check_second_client_route (route);

  /* Set the first route as current */
  set_current_route (test, 0);
  route = trp_client_navigation_get_current_route (test->client);
  check_first_client_route (route);

  /* Try setting an invalid route as current */
  test->wait_count = 1;
  trp_client_mock_set_current_route_async (test->mock, 10, remove_route_cb, test);
  g_main_loop_run (test->loop);
  g_assert_error (test->error, G_DBUS_ERROR, G_DBUS_ERROR_INVALID_ARGS);
  g_assert (!test->result);
}

/* test ClearRoutes() */
static void
test_clear_routes (Test *test,
                   gconstpointer unused)
{
  TrpClientRoute *route;
  GPtrArray *routes;

  init_client (test);
  init_mock (test);

  g_signal_connect (test->client, "notify::routes", G_CALLBACK (notify_cb), test);

  /* Add 2 routes */
  add_route (test, create_first_mock_route (test));
  g_assert_cmpint (test->index, ==, 0);

  add_route (test, create_second_mock_route (test));
  g_assert_cmpint (test->index, ==, 1);

  route = trp_client_navigation_get_current_route (test->client);
  g_assert (route == NULL);

  /* Set the second route as current */
  g_signal_connect (test->client, "notify::current-route", G_CALLBACK (notify_cb), test);

  set_current_route (test, 1);
  route = trp_client_navigation_get_current_route (test->client);
  check_second_client_route (route);

  /* Clear all routes */

  /* Wait for clear_routes_cb and notify_cb twice (routes and current-route) */
  test->wait_count = 3;
  trp_client_mock_clear_routes_async (test->mock, clear_routes_cb, test);
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
  g_assert (test->result);

  routes = trp_client_navigation_get_routes (test->client);
  g_assert_cmpuint (routes->len, ==, 0);
  route = trp_client_navigation_get_current_route (test->client);
  g_assert (route == NULL);
}

static void
init_client_progress (Test *test)
{
  g_async_initable_init_async (G_ASYNC_INITABLE (test->client_progress),
                               G_PRIORITY_DEFAULT, NULL,
                               client_init_cb, test);
  g_main_loop_run (test->loop);
  g_assert_no_error (test->error);
}

static void
set_start_time_cb (GObject *source,
                   GAsyncResult *result,
                   gpointer user_data)
{
  Test *test = user_data;

  test->result = trp_client_mock_set_start_time_finish (TRP_CLIENT_MOCK (source),
                                                        result, &test->error);

  test->wait_count--;
  if (test->wait_count == 0)
    g_main_loop_quit (test->loop);
}

/* Set the progress start time */
static void
test_set_start_time (Test *test,
                     gconstpointer unused)
{
  g_autoptr (GDateTime) d = NULL;

  init_client_progress (test);
  init_mock (test);

  g_signal_connect (test->client_progress, "notify::start-time",
                    G_CALLBACK (notify_cb), test);

  /* Set a start time */
  d = g_date_time_new_from_unix_utc (123456789);

  test->wait_count = 2; /* set_estimated_end_time_cb and notify_cb */
  trp_client_mock_set_start_time_async (test->mock, d, set_start_time_cb, test);
  g_main_loop_run (test->loop);
  g_assert (test->result);
  g_assert_no_error (test->error);

  /* Unset the start time */
  test->wait_count = 2; /* set_estimated_end_time_cb and notify_cb */
  trp_client_mock_set_start_time_async (test->mock, NULL, set_start_time_cb, test);
  g_main_loop_run (test->loop);
  g_assert (test->result);
  g_assert_no_error (test->error);
}

static void
set_estimated_end_time_cb (GObject *source,
                           GAsyncResult *result,
                           gpointer user_data)
{
  Test *test = user_data;

  test->result = trp_client_mock_set_estimated_end_time_finish (TRP_CLIENT_MOCK (source),
                                                                result, &test->error);

  test->wait_count--;
  if (test->wait_count == 0)
    g_main_loop_quit (test->loop);
}

/* Set the progress estimated end time */
static void
test_set_estimated_end_time (Test *test,
                             gconstpointer unused)
{
  g_autoptr (GDateTime) d = NULL;

  init_client_progress (test);
  init_mock (test);

  g_signal_connect (test->client_progress, "notify::estimated-end-time",
                    G_CALLBACK (notify_cb), test);

  /* Set a estimated end time */
  d = g_date_time_new_from_unix_utc (123456789);

  test->wait_count = 2; /* set_estimated_end_time_cb and notify_cb */
  trp_client_mock_set_estimated_end_time_async (test->mock, d, set_estimated_end_time_cb, test);
  g_main_loop_run (test->loop);
  g_assert (test->result);
  g_assert_no_error (test->error);

  /* Unset the estimated end time */
  test->wait_count = 2; /* set_estimated_end_time_cb and notify_cb */
  trp_client_mock_set_estimated_end_time_async (test->mock, NULL, set_estimated_end_time_cb, test);
  g_main_loop_run (test->loop);
  g_assert (test->result);
  g_assert_no_error (test->error);
}

int
main (int argc, char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/mock-service/activation/TrpClientService", Test, NULL,
              setup, test_activation_client_service, teardown);
  g_test_add ("/mock-service/activation/TrpClientMock", Test, NULL,
              setup, test_activation_client_mock, teardown);
  g_test_add ("/mock-service/add-route", Test, NULL,
              setup, test_add_route, teardown);
  g_test_add ("/mock-service/remove-route", Test, NULL,
              setup, test_remove_route, teardown);
  g_test_add ("/mock-service/set-current-route", Test, NULL,
              setup, test_current_route, teardown);
  g_test_add ("/mock-service/clear-routes", Test, NULL,
              setup, test_clear_routes, teardown);
  g_test_add ("/mock-service/set-start-time", Test, NULL,
              setup, test_set_start_time, teardown);
  g_test_add ("/mock-service/set-estimated-end-time", Test, NULL,
              setup, test_set_estimated_end_time, teardown);

  return g_test_run ();
}
