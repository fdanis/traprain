/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_MOCK_SERVICE_H__
#define __TRAPRAIN_MOCK_SERVICE_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define TRP_MOCK_TYPE_SERVICE (trp_mock_service_get_type ())
G_DECLARE_FINAL_TYPE (TrpMockService, trp_mock_service, TRP_MOCK, SERVICE, GObject)

TrpMockService *trp_mock_service_new (void);

void trp_mock_service_run (TrpMockService *self,
                           gboolean use_session_bus,
                           GError **error);

G_END_DECLS

#endif /* __TRAPRAIN_MOCK_SERVICE_H__ */
